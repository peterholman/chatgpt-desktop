Access ChatGPT from anywhere, anytime!

# Shortcuts

This app turns you into a Trello power-user with these shortcuts:
- Show/hide: Ctrl+Alt+X
- Add a card: Ctrl+Alt+C
- Copy the first list to clipboard: Ctrl+Alt+.

# Install

On Linux, install from: https://snapcraft.io/chatgpt-desktop/

You can also install from the CLI with: `snap install chatgpt-desktop`

# Release notes


## 1.0.4 (2023-04-23)

- Fixed auto-reloading stopped working again.

## 1.0.3 (2023-03-22)

- Fixed auto-reload when error encountered stopped working.

## 1.0.2 (2023-03-01)

- General bug fixes. Should be more stable now.
- After this release, user settings won't be cleared after each update.
- Fixed "Reset app data" not resetting zoom sometimes.
- Made Shift+Enter make a new line.
- Fixed app sometimes crashes when you do "Reset app data"
- Added an error message when shortcuts can't be registered.
- Added a "Home" menu item under the "Help" menu.
- Fixed "Reset app data" just quits the app sometimes.
- Fixed the capatalization of: "desktop"

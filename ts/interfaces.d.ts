interface Point
{
    x: number,
    y: number,
}

interface WindowProperties
{
    pageUrl: string,
    boundsObj: Electron.Rectangle,
}

interface DefaultSettings
{
    isRunAtStartup: boolean,
    isAlwaysShowMenuBar: boolean,
    windowPropertiesObj: WindowProperties,
}

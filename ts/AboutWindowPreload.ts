const {ipcRenderer, contextBridge} = require(`electron`);

contextBridge.exposeInMainWorld(`electronIpcRenderer`, {
    onMessageReceived: (messageCode: string, listenerFunc: (evt: Event, ...argsArr: any[]) => void): any => ipcRenderer.on(messageCode, listenerFunc), // Allow any.
    sendMessage:       ipcRenderer.send,
    invokeMessage:     ipcRenderer.invoke,
});

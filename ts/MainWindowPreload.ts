import {ipcRenderer} from 'electron';

var $: any;
let sendLastChatText    = ``;
let restoreLastChatText = ``;

// Called when window is shown.
ipcRenderer.on(`focusInput`, function (): void
{
    if (window.location.href.startsWith(`https://chat.openai.com`)) {
        document.getElementsByTagName(`textarea`)[0].focus();
    }
});

ipcRenderer.on(`newChat`, function (): void
{
    if (window.location.href.startsWith(`https://chat.openai.com`)) {
        if ($(`nav`).length) { // Side menu is popped out.
            $(`nav > a:contains(New chat)`).get(0).click();
        } else {
            $(`button`).get(1).click(); // Not a great selector, but it's all I've got.
        }
    }
});

ipcRenderer.invoke(`getLastChat`).then(function (lastChatText: string): void
{
    if (lastChatText) {
        restoreLastChatText = lastChatText;
    }
});

function addTextareaFocusEvent(): void
{
    $(document).on(`focus`, `textarea:first`, function (this: HTMLTextAreaElement): void
    {
        if (restoreLastChatText) {
            this.value = restoreLastChatText;

            // Something clears the value afterwards, so keep restoring it until it sticks.
            let triesCount   = 0;
            const intervalId = setInterval(function (): void
            {
                const $textareaJq = $(`textarea:first`);
                if ($textareaJq.val()) {
                    clearInterval(intervalId);
                    ipcRenderer.send(`lastChatInserted`);
                    restoreLastChatText = ``;

                    // Send chat.
                    $textareaJq.next(`button`).trigger(`click`);
                } else {
                    $textareaJq.val(restoreLastChatText);
                    triesCount++;
                    if (triesCount > 20) {
                        clearInterval(intervalId);
                    }
                }
            }, 200);
        }
    });
}

function addTextareaKeydownEvent(): void
{
    $(document).on(`keydown`, `textarea:first`, function (this: HTMLTextAreaElement, evt: JQueryKeyEventObject): void
    {
        if (evt.key == `Enter`) {
            if (evt.ctrlKey || evt.shiftKey) {
                evt.preventDefault();
                evt.stopImmediatePropagation(); // Regular stopPropagation() doesn't work.

                // Add line break.
                this.value = `${this.value}\n`;
                this.dispatchEvent(new Event(`input`, {'bubbles': true})); // Without this, the textarea doesn't get taller.
            } else {
                // Send chat.
                evt.preventDefault();
                // Next element is the button element.
                (<HTMLButtonElement>this.nextElementSibling).click(); // Triggers event below.
            }
        }
    });
}

function addSendButtonClickEvent(): void
{
    // If the user clicks the send button, reload if there's an error.
    $(document).on(`click`, `textarea:first + button`, function (): void
    {
        sendLastChatText = $(`textarea:first`).val();

        // Reload if there's an error.
        checkForOpenAIError().then(function (hasError: boolean): void
        {
            if (hasError) {
                ipcRenderer.send(`openAIError`, sendLastChatText);
            }
        });
    });
}

// To cause an error, delete the `cf_clearance` cookie before sending a message.
function openAIHasError(): boolean
{
    return Boolean($(`main:contains(An error occurred)`).length) || Boolean($(`main:contains(Something went wrong)`).length);
}

function checkForOpenAIError(): Promise<boolean>
{
    if (openAIHasError()) {
        return Promise.resolve(true);
    } else {
        return new Promise(function (resolveFunc: (hasError: boolean) => void): void
        {
            let triesCount   = 0;
            const intervalId = setInterval(function (): void
            {
                if (openAIHasError()) {
                    resolveFunc(true);
                    clearInterval(intervalId);
                } else {
                    triesCount++;
                    if (triesCount > 20) {
                        clearInterval(intervalId);
                        resolveFunc(false);
                    }
                }
            }, 200);
        });
    }
}

window.addEventListener(`load`, function (): void
{
    $ = require(`jquery`);
    if (window.location.href.startsWith(`https://chat.openai.com`)) {
        addTextareaFocusEvent();
        addTextareaKeydownEvent();
        addSendButtonClickEvent();
    }
});

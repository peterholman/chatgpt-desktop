import {app, session, BrowserWindow, shell, Menu, dialog} from 'electron';
import {MainApp} from './MainApp';
import {MainWindow} from './MainWindow';
import {AboutWindow} from './AboutWindow';
import path = require('path');

export class MenuBar
{

    private static openMenuItemObj: Electron.MenuItemConstructorOptions;
    private static showHideMenuItemObj: Electron.MenuItemConstructorOptions;
    private static newChatMenuItemObj: Electron.MenuItemConstructorOptions;
    private static quitMenuItemObj: Electron.MenuItemConstructorOptions;
    private static resetAppDataItemObj: Electron.MenuItemConstructorOptions;
    private static reportIssueItemObj: Electron.MenuItemConstructorOptions;
    private static homeItemObj: Electron.MenuItemConstructorOptions;
    private static aboutItemObj: Electron.MenuItemConstructorOptions;
    private static runAtStartupItemObj: Electron.MenuItemConstructorOptions;
    private static isAlwaysShowMenuBarItemObj: Electron.MenuItemConstructorOptions;

    public static init(): void
    {
        MenuBar.openMenuItemObj            = {
            label:               `Open ChatGPT`,
            accelerator:         MainApp.shortcutStr_showHide,
            icon:                path.join(MainApp.appPath, `assets`, `menuIcons`, `logo.png`),
            registerAccelerator: false,
            click:               function (): void
                                 {
                                     MainWindow.showHideMainWin(true);
                                 }
        };
        MenuBar.showHideMenuItemObj        = {
            label:               `Show / hide`,
            accelerator:         MainApp.shortcutStr_showHide,
            icon:                path.join(MainApp.appPath, `assets`, `menuIcons`, `logo.png`),
            registerAccelerator: false,
            click:               function (): void
                                 {
                                     MainWindow.showHideMainWin();
                                 }
        };
        MenuBar.newChatMenuItemObj         = {
            label:               `New chat`,
            accelerator:         MainApp.shortcutStr_newChat,
            icon:                path.join(MainApp.appPath, `assets`, `menuIcons`, `add.png`),
            registerAccelerator: false,
            click:               function (): void
                                 {
                                     MainApp.newChat();
                                 }
        };
        MenuBar.quitMenuItemObj            = {
            label: `Quit`,
            click: function (): void
                   {
                       app.quit();
                   }
        };
        MenuBar.resetAppDataItemObj        = {
            label: `Reset app data`,
            click: function (): void
                   {
                        dialog.showMessageBox(
                            MainWindow.windowObj,
                            {
                                type:      `question`,
                                buttons:   [`Yes ... reset`, `Cancel`],
                                detail:    `All your preferences are permanently wiped, you'll be logged out, and ChatGPT Desktop restarts.`,
                                defaultId: 0,
                                cancelId:  1,
                                message:   `Reset app data?`,
                                icon:      MainApp.appIconPath, // On Linux this icon looks blurry on a high density display. Couldn't fix it.
                            }
                        ).then(
                            function (responseObj: Electron.MessageBoxReturnValue): void
                            {
                                if (responseObj.response == 0) {
                                    MainApp.electronStoreObj.clear();

                                    session.defaultSession.clearCache();
                                    session.defaultSession.clearStorageData();

                                    app.relaunch();
                                    app.exit();
                                }
                            }
                        );
                   }
        };
        MenuBar.reportIssueItemObj         = {
            label: `Report issue...`,
            click: function (): void
                   {
                       shell.openExternal(`https://gitlab.com/joshuaredmond/chatgpt-dekstop/-/issues`);
                   }
        };
        MenuBar.homeItemObj               = {
            label: `Home`,
            click: function (): void
                   {
                       MainWindow.windowObj.loadURL(MainApp.applicationUrl);
                   }
        };
        MenuBar.aboutItemObj               = {
            label: `About`,
            click: function (): void
                   {
                       AboutWindow.open();
                   }
        };
        MenuBar.runAtStartupItemObj        = {
            label:   `Run at startup`,
            type:    `checkbox`,
            checked: MainApp.electronStoreObj.get(`isRunAtStartup`),
            visible: MainApp.isRunAtStartupFeatureEnabled,
            click:   function (): void
                     {
                         MainApp.electronStoreObj.set(`isRunAtStartup`, !MainApp.electronStoreObj.get(`isRunAtStartup`));
                         if (MainApp.isPackaged) {
                             MainApp.setRunAtStartup(MainApp.electronStoreObj.get(`isRunAtStartup`));
                         }
                     }
        };
        MenuBar.isAlwaysShowMenuBarItemObj = {
            label:   `Always show menu bar`,
            type:    `checkbox`,
            checked: MainApp.electronStoreObj.get(`isAlwaysShowMenuBar`),
            visible: [`lin`, `win`].includes(MainApp.platformType),
            click:   function (): void
                     {
                         MainApp.electronStoreObj.set(`isAlwaysShowMenuBar`, !MainApp.electronStoreObj.get(`isAlwaysShowMenuBar`));
                         MainWindow.windowObj.setAutoHideMenuBar(!MainApp.electronStoreObj.get(`isAlwaysShowMenuBar`));
                         MainWindow.windowObj.setMenuBarVisibility(MainApp.electronStoreObj.get(`isAlwaysShowMenuBar`));
                     }
        };
        Menu.setApplicationMenu(Menu.buildFromTemplate(MenuBar.getApplicationMenu()));
    }

    public static getTrayIconMenu(): Electron.MenuItemConstructorOptions[]
    {
        return [
            MenuBar.newChatMenuItemObj,
            MenuBar.openMenuItemObj,
            {type: `separator`},
            MenuBar.aboutItemObj,
            MenuBar.reportIssueItemObj,
            MenuBar.resetAppDataItemObj,
            {type: `separator`},
            MenuBar.quitMenuItemObj,
        ];
    }

    public static generateRightClickMenu(): Electron.MenuItemConstructorOptions[]
    {
        return [
            {role: `copy`, accelerator: `CmdOrCtrl+C`},
            {role: `paste`, accelerator: `CmdOrCtrl+X`},
        ];
    }

    public static getApplicationMenu(): Electron.MenuItemConstructorOptions[]
    {
        var applicationMenuArr: Electron.MenuItemConstructorOptions[] = [
            {
                label: `File`,
                submenu:
                       [
                           MenuBar.newChatMenuItemObj,
                           MenuBar.showHideMenuItemObj,
                           {type: `separator`},
                           {role: `quit`},
                       ]
            },
            {
                label:   `Edit`,
                submenu: [
                    // @formatter:off
                    {label: `Undo`,                     role: `undo`,                   accelerator: `CmdOrCtrl+Z`},
                    {label: `Redo`,                     role: `redo`,                   accelerator: `CmdOrCtrl+Y`},
                    {type: `separator`},
                    {label: `Cut`,                      role: `cut`,                    accelerator: `CmdOrCtrl+X`},
                    {label: `Copy`,                     role: `copy`,                   accelerator: `CmdOrCtrl+C`},
                    {label: `Paste`,                    role: `paste`,                  accelerator: `CmdOrCtrl+V`},
                    {label: `Paste and match style`,    role: `pasteAndMatchStyle`,     accelerator: `CmdOrCtrl+Shift+V`},
                    {label: `Select all`,               role: `selectAll`,              accelerator: `CmdOrCtrl+A`},
                    // @formatter:on

                    // Print doesn't work from an installed snap. Looks hard to get working. See: https://snapcraft.io/docs/cups-interface
                    // {
                    //     label:       `Print`,
                    //     accelerator: `CmdOrCtrl+P`,
                    //     click:       function (item, focusedWindow?: BrowserWindow): void
                    //                  {
                    //                      if (focusedWindow) {
                    //                          focusedWindow.webContents.print({silent: false, printBackground: true});
                    //                      }
                    //                  }
                    // },
                ]
            },
            {
                label:   `View`,
                submenu: [
                    {role: `reload`},
                    {role: `forceReload`},
                    {role: `reload`, visible: false, accelerator: `F5`},
                    {type: `separator`},
                    {role: `togglefullscreen`},
                    {type: `separator`},
                    {label: `Zoom in`, role: `zoomIn`, accelerator: `CmdOrCtrl+=`},
                    {label: `Zoom out`, role: `zoomOut`, accelerator: `CmdOrCtrl+-`},
                    {label: `Actual size`, role: `resetZoom`, accelerator: `CmdOrCtrl+0`},
                ]
            },
            {
                label:   `History`,
                submenu: [
                    {
                        label:       `Forward`,
                        accelerator: `Alt+Right`,
                        click:       function (item, focusedWindow?: BrowserWindow): void
                                     {
                                         if (focusedWindow) {
                                             focusedWindow.webContents.goForward();
                                         }
                                     }
                    },
                    {
                        label:       `Back`,
                        accelerator: `Alt+Left`,
                        click:       function (item, focusedWindow?: BrowserWindow): void
                                     {
                                         if (focusedWindow) {
                                             focusedWindow.webContents.goBack();
                                         }
                                     }
                    }
                ]
            },
            {
                label:   `Window`,
                submenu: [
                    MenuBar.runAtStartupItemObj,
                    MenuBar.isAlwaysShowMenuBarItemObj,
                    {type: `separator`},
                    {role: `minimize`},
                    {
                        role:                `close`,
                        accelerator:         MainApp.shortcutStr_showHide,
                        registerAccelerator: false,
                    }
                ]
            },
            {
                label:   `Help`,
                submenu: [
                    MenuBar.homeItemObj,
                    MenuBar.aboutItemObj,
                    {type: `separator`},
                    MenuBar.reportIssueItemObj,
                    MenuBar.resetAppDataItemObj,
                ]
            },
        ];
        if (!MainApp.isPackaged && applicationMenuArr[0][`submenu`]) {
            (<Electron.MenuItemConstructorOptions[]>applicationMenuArr[0][`submenu`]).push(
                {
                    visible:     false,
                    label:       `Open dev tools`,
                    accelerator: `F12`,
                    role:        `toggleDevTools`,
                }
            );
            (<Electron.MenuItemConstructorOptions[]>applicationMenuArr[0][`submenu`]).push(
                {
                    visible:     false,
                    label:       `Open dev tools`,
                    accelerator: `Ctrl+Shift+I`,
                    role:        `toggleDevTools`,
                }
            );
        }
        return applicationMenuArr;
    }

}


import path = require('path');

import {Menu, Tray} from 'electron';
import {MainWindow} from './MainWindow';
import {MenuBar} from './MenuBar';
import {MainApp} from "./MainApp";

export class TrayIcon
{

    public static trayIconObj: Tray;

    public static init(): void
    {
        TrayIcon.trayIconObj = new Tray(path.join(MainApp.appPath, `assets`, `icon.png`));
        TrayIcon.setTrayMenu();
        TrayIcon.trayIconObj.on(`click`, function (): void
        {
            MainWindow.showHideMainWin(true);
        });
        TrayIcon.trayIconObj.on(`double-click`, function (): void
        {
            MainWindow.showHideMainWin(true);
        });
    }

    public static setTrayMenu(): void
    {
        if (TrayIcon.trayIconObj) { // I got a Rollbar error about this being undefined, no idea why.
            TrayIcon.trayIconObj.setContextMenu(Menu.buildFromTemplate(MenuBar.getTrayIconMenu()));
        }
    }
}

import path from 'path';
import {BrowserWindow, shell} from 'electron';
import {MainApp} from './MainApp';
import {MainWindow} from './MainWindow';

export class AboutWindow
{

    public static aboutWin: BrowserWindow | null;

    private static init(): void
    {
        var isDebug          = false;
        var widthPx          = (isDebug) ? 1500 : 23 * 16;
        var heightPx         = (isDebug) ? 1000 : 40 * 16;
        var boundsObj        = MainWindow.getCenteredWindowBounds(widthPx, heightPx);
        AboutWindow.aboutWin = new BrowserWindow({
            width:           boundsObj.width,
            height:          boundsObj.height,
            x:               boundsObj.x,
            y:               boundsObj.y,
            resizable:       isDebug,
            minimizable:     false,
            icon:            MainApp.appIconPath,
            title:           `About ChatGPT Desktop`,
            backgroundColor: `rgb(249, 249, 249)`,
            webPreferences:  {
                devTools: isDebug,
                preload:  path.join(__dirname, `AboutWindowPreload.js`),
            },
        });
        AboutWindow.aboutWin.removeMenu();
        AboutWindow.aboutWin.webContents.ipc.handle(`versionStr`, async function (): Promise<string>
        {
            return MainApp.packageJsonObj.version;
        });
        AboutWindow.aboutWin.webContents.ipc.on(`closeAboutWin`, function (): void
        {
            AboutWindow.aboutWin?.close();
        });
        AboutWindow.aboutWin.on(`closed`, function (): void
        {
            AboutWindow.aboutWin = null;
        });

        if (isDebug) {
            AboutWindow.aboutWin.on(`resize`, function (): void
            {
                console.log(AboutWindow.aboutWin?.getSize()); // Allow `console.log`.
            });
        }
        AboutWindow.aboutWin.webContents.on(`will-navigate`, function (evt: Electron.Event, navigateUrl: string): void
        {
            evt.preventDefault();
            shell.openExternal(navigateUrl);
        });
        AboutWindow.aboutWin.loadURL(`file://${__dirname}/../aboutWindow.html`);
    }

    public static open(): void
    {
        if (AboutWindow.aboutWin) {
            AboutWindow.aboutWin.show();
        } else {
            AboutWindow.init();
        }
    }

}

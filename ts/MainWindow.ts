import {BrowserWindow, dialog, Menu, screen, shell} from 'electron';
import {MainApp} from './MainApp';
import {MenuBar} from './MenuBar';
import {TrayIcon} from './TrayIcon';
import path = require('path');

export class MainWindow
{

    public static windowObj: BrowserWindow;
    public static defaultWidthPx  = 500;
    public static defaultHeightPx = 700;
    private static lastChatText   = ``;

    public static init(): void
    {
        var windowPropertiesObj = MainApp.electronStoreObj.get(`windowPropertiesObj`);
        if (windowPropertiesObj && Object.hasOwn(windowPropertiesObj, `pageUrl`)) {
            MainWindow.createWindow(windowPropertiesObj);
        } else {
            MainWindow.createWindow({
                'pageUrl':   MainApp.applicationUrl,
                'boundsObj': MainWindow.getCenteredWindowBounds(MainWindow.defaultWidthPx, MainWindow.defaultHeightPx),
            });
        }
    }

    public static createWindow(windowPropertiesObj: WindowProperties): void
    {
        MainWindow.windowObj = new BrowserWindow({
            title:           MainApp.packageJsonObj.productName,
            x:               windowPropertiesObj.boundsObj.x,
            y:               windowPropertiesObj.boundsObj.y,
            width:           windowPropertiesObj.boundsObj.width,
            height:          windowPropertiesObj.boundsObj.height,
            icon:            MainApp.appIconPath,
            autoHideMenuBar: !MainApp.electronStoreObj.get(`isAlwaysShowMenuBar`),
            show:            !MainApp.isQuietMode,
            webPreferences:  {
                preload:         path.join(__dirname, `MainWindowPreload.js`),
                devTools:        !MainApp.isPackaged,
                nodeIntegration: true,
            }
        });
        MainWindow.windowObj.loadURL(windowPropertiesObj.pageUrl || MainApp.applicationUrl);
        MainWindow.setupWindowEvents();
    }

    private static setupWindowEvents(): void
    {
        MainWindow.windowObj.on(`close`, function (evt): void
        {
            if (!MainApp.isQuitting) {
                evt.preventDefault();
                MainWindow.windowObj.hide();
            }
        });
        // @formatter:off
        MainWindow.windowObj.on(`move`,                             function (                          ): void { MainWindow.saveWindowProperties();                         });
        MainWindow.windowObj.on(`resize`,                           function (                          ): void { MainWindow.saveWindowProperties();                         });
        MainWindow.windowObj.webContents.on(`did-navigate`,         function (                          ): void { MainWindow.saveWindowProperties(); TrayIcon.setTrayMenu(); });
        MainWindow.windowObj.webContents.on(`did-navigate-in-page`, function (                          ): void { MainWindow.saveWindowProperties(); TrayIcon.setTrayMenu(); });
        // @formatter:on
        MainWindow.windowObj.on(`focus`, function (): void
        {
            MainWindow.windowObj.webContents.send(`focusInput`);
        });
        MainWindow.windowObj.webContents.on(`context-menu`, function (): void
        {
            // On Mac (darwin) it doesn`t focus the window if you right-click on it.
            if (MainApp.platformType == `mac`) {
                MainWindow.windowObj.focus();
            }
            Menu.buildFromTemplate(MenuBar.generateRightClickMenu()).popup({});
        });
        MainWindow.windowObj.webContents.setWindowOpenHandler(function (detailsObj): { action: `deny` }
        {
            if (detailsObj.url.startsWith(MainApp.applicationUrl)) {
                MainWindow.createWindow({
                    'pageUrl':   detailsObj.url,
                    'boundsObj': MainWindow.getCenteredWindowBounds(MainWindow.defaultWidthPx, MainWindow.defaultHeightPx),
                });
            } else {
                shell.openExternal(detailsObj.url);
            }
            return {action: `deny`};
        });
        MainWindow.windowObj.webContents.on(`will-navigate`, function (evt, navigateUrl): void
        {
            if (!MainWindow.containsAny(navigateUrl, MainApp.urlContainsAllowlistArr)) {
                shell.openExternal(navigateUrl);
                evt.preventDefault();
            }
        });
        MainWindow.windowObj.webContents.ipc.on(`openAIError`, function (evt: Electron.IpcMainEvent, lastChatText: string): void
        {
            if (MainApp.isPackaged) {
                MainApp.rollbarObj.error(`OpenAI Error.`);
            }
            MainWindow.lastChatText = lastChatText;

            // Show dialog while reloading.
            const abortControllerObj = new AbortController();
            dialog.showMessageBox(MainWindow.windowObj, { // Can't take off all buttons.
                type:    `info`,
                icon:    path.join(MainApp.appPath, `assets`, `icon.png`),
                title:   `Reloading...`,
                message: `Reloading... please wait.`,
                detail:  `OpenAI Error detected.`,
                signal:  abortControllerObj.signal,
            });
            MainWindow.windowObj.reload();
            // Hide dialog once load finishes. Though, it turns out it isn't loading for very long.
            MainWindow.windowObj.webContents.once(`did-stop-loading`, function (): void
            {
                abortControllerObj.abort();
            });
        });
        MainWindow.windowObj.webContents.ipc.handle(`getLastChat`, async function (): Promise<string>
        {
            return MainWindow.lastChatText;
        });
        MainWindow.windowObj.webContents.ipc.on(`lastChatInserted`, function (): void
        {
            MainWindow.lastChatText = ``;
        });
    }

    public static containsAny(haystackStr: string, needlesArr: string[]): boolean
    {
        return needlesArr.some((urlContainsStr): boolean => haystackStr.includes(urlContainsStr));
    }

    public static getCenteredWindowBounds(widthPx: number, heightPx: number): Electron.Rectangle
    {
        var mainScreenBoundsObj = screen.getDisplayNearestPoint(screen.getCursorScreenPoint()).bounds;
        var winX                = mainScreenBoundsObj.x + ((mainScreenBoundsObj.width - widthPx) / 2);
        var winY                = mainScreenBoundsObj.y + ((mainScreenBoundsObj.height - heightPx) / 2);
        return {
            'width':  widthPx,
            'height': heightPx,
            'x':      Math.round(winX), // Math.round needed since it throws a strange error without it.
            'y':      Math.round(winY) // Math.round needed since it throws a strange error without it.
        };
    }

    /*
     * Save all the window's state, for app re-opening.
     * Can't call in `before-quit` because `before-quit` isn't triggered from a Ctrl+C in the terminal.
     */
    public static saveWindowProperties(): void
    {
        const pageUrl = MainWindow.windowObj.webContents.getURL();
        if (MainWindow.containsAny(pageUrl, MainApp.dontSaveUrlListArr)) {
            // TODO: Move `pageUrl` out of `windowPropertiesObj` and make `windowPropertiesObj` just store the bounds.
            // Sometimes this causes errors.
            const windowPropertiesObj = MainApp.electronStoreObj.get(`windowPropertiesObj`);
            MainApp.electronStoreObj.set(`windowPropertiesObj`, {
                'pageUrl':   windowPropertiesObj.pageUrl, // Don't update URL.
                'boundsObj': MainWindow.windowObj.getBounds(),
            });
        } else {
            MainApp.electronStoreObj.set(`windowPropertiesObj`, {
                'pageUrl':   pageUrl,
                'boundsObj': MainWindow.windowObj.getBounds(),
            });
        }
    }

    public static showHideMainWin(isShow?: boolean): void
    {
        if (
            ( // If `isShow` is passed in, use that instead.
                isShow !== undefined
                &&
                isShow
            )
            ||
            (
                isShow === undefined
                &&
                (
                    !MainWindow.windowObj.isVisible()
                    ||
                    !MainWindow.windowObj.isFocused()
                )
            )
        ) {
            MainWindow.windowObj.show();
        } else {
            MainWindow.windowObj.hide();
        }
    }

}

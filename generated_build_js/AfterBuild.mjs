import 'source-map-support/register.js'; // This is makes error messages use the .ts file path instead. Very handy.
import fs from 'fs-extra';
import path from 'path';
export class AfterBuild {
    static moveExecutable(binaryPath) {
        var newBinaryPath = `installableBinaries/${path.basename(binaryPath)}`;
        console.log(`   - Moving binary to: ${newBinaryPath}`); // Keep for debugging.
        fs.rename(binaryPath, newBinaryPath, function (errorObj) {
            if (errorObj) {
                throw errorObj;
            }
        });
        return newBinaryPath;
    }
    static deleteDistFolder() {
        console.log(`   - Deleting /dist folder.`); // Keep for debugging.
        fs.rm(`dist`, { recursive: true });
    }
}
//# sourceMappingURL=AfterBuild.mjs.map
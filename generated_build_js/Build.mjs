import 'source-map-support/register.js'; // This is makes error messages use the .ts file path instead. Very handy.
import { BeforeBuild } from './BeforeBuild.mjs';
import { AfterBuild } from './AfterBuild.mjs';
import builder from 'electron-builder';
import chalk from 'chalk';
// @ts-ignore because this file is outside the ts_build folder.
// Allow eslint-disable because eslint hasn't caught up to the new assert syntax.
// eslint-disable-next-line @typescript-eslint/quotes
import packageJsonObj from '../package.json' assert { type: 'json' };
export class Build {
    static run() {
        Build.buildType = process.argv[2]; // 'dist', 'release'.
        Build.platformStr = process.argv[3];
        Build.outputType = process.argv[4]; // 'deb', 'snap', 'exe', 'appx', 'dmg'.
        console.log(`\nPrepare installer. ${chalk.dim(`${packageJsonObj.productName} v${packageJsonObj.version}. Electron ${packageJsonObj.devDependencies.electron}.`)}`); // Keep for debugging.
        if (Build.buildType == `release`) {
            BeforeBuild.incrementVersion(packageJsonObj.version);
        }
        console.log(`\nCreate binary. ${chalk.dim(`Electron Builder ${packageJsonObj.devDependencies[`electron-builder`]}.`)}`); // Keep for debugging.
        Build.createBinary(function (binaryPath) {
            console.log(`\nFinishing build.`); // Keep for debugging.
            var newBinaryPath = AfterBuild.moveExecutable(binaryPath);
            AfterBuild.deleteDistFolder();
            console.log(`\n\n${chalk.bold.bgGreen(`   Build finished!   `)}  ${chalk.dim(`Created: ${newBinaryPath}`)}\n\n`);
            if (Build.platformStr == `lin` && Build.outputType == `snap`) {
                console.log(`To install the .snap for testing:`); // Keep for debugging.
                console.log(`\nsnap remove --purge ${packageJsonObj.name}`); // Keep for debugging.
                console.log(`snap install --devmode ${newBinaryPath}\n`); // Keep for debugging.
                console.log(`To release the .snap to the snap store:`); // Keep for debugging.
                console.log(`    1. Upload the .snap as a revision:\n\nsnapcraft upload --release=latest/edge ${newBinaryPath}\n`); // Keep for debugging.
                console.log(`    2. Open: https://snapcraft.io/chat-gpt/releases`); // Keep for debugging.
                console.log(`    3. Promote the revision to the stable channel.`); // Keep for debugging.
            }
        });
    }
    /*
     * `electron-builder` is installed with NPM right now, despite Yarn being the recommended package manager. See: https://www.electron.build/#installation
     */
    static createBinary(callbackFunc) {
        console.log(`   - Creating binary...`); // Keep for debugging.
        /**
         * @see https://www.electron.build/configuration/configuration
         */
        const optionsObj = {
            "asar": true,
            "files": [
                `!dist`,
                `!screenshots`,
                `!installableBinaries`,
                `!ts`,
                `!ts_build`,
                `!generated_build_js`,
            ],
            "linux": {
                "target": Build.outputType,
                "artifactName": `ChatGPTDesktop-\${version}-lin.\${ext}`,
                "category": `Utility`,
                "description": `Access ChatGPT from anywhere, anytime!`, // Required for snaps.
            },
            "snap": {
                "autoStart": true,
            },
        };
        builder.build({
            targets: builder.Platform.LINUX.createTarget(),
            config: optionsObj,
        })
            .then((binaryPathsArr) => {
            callbackFunc(binaryPathsArr[binaryPathsArr.length - 1]); // On Windows it creates a .blockmap file which is the first element in the array. Always get last element.
        })
            .catch((error) => {
            console.error(error);
        });
    }
}
Build.run();
//# sourceMappingURL=Build.mjs.map
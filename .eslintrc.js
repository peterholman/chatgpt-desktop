// Allow eslint-disable because it works.
// eslint-disable-next-line no-undef
module.exports = {
    'root':           true,
    'parser':         `@typescript-eslint/parser`,
    'plugins':        [
        `@typescript-eslint`,
    ],
    'extends':        [
        `eslint:recommended`,
        `plugin:@typescript-eslint/recommended`,
    ],
    'env':            {
        'browser': true
    },
    'ignorePatterns': [`generated/js/**/*.js`, `*.json`],
    'rules':          {
        'no-var':                                            `off`,
        'no-useless-escape':                                 `off`,
        'prefer-rest-params':                                `off`, // Because not supported in IE.
        'prefer-spread':                                     `off`, // Because not supported in IE.
        'prefer-template':                                   `warn`,
        'quotes':                                            `off`,
        '@typescript-eslint/quotes':                         [`error`, `backtick`],
        '@typescript-eslint/ban-ts-comment':                 `off`,
        '@typescript-eslint/no-var-requires':                `off`,
        '@typescript-eslint/no-explicit-any':                `off`,
        '@typescript-eslint/no-inferrable-types':            `off`,
        '@typescript-eslint/explicit-module-boundary-types': `off`,
        '@typescript-eslint/no-namespace':                   `off`,
        '@typescript-eslint/no-unused-vars':                 `error`,
        '@typescript-eslint/explicit-function-return-type':  [
            `error`,
            {
                'allowTypedFunctionExpressions': false,
            }
        ],
    }
};

"use strict";
var __awaiter = (this && this.__awaiter) || function (thisArg, _arguments, P, generator) {
    function adopt(value) { return value instanceof P ? value : new P(function (resolve) { resolve(value); }); }
    return new (P || (P = Promise))(function (resolve, reject) {
        function fulfilled(value) { try { step(generator.next(value)); } catch (e) { reject(e); } }
        function rejected(value) { try { step(generator["throw"](value)); } catch (e) { reject(e); } }
        function step(result) { result.done ? resolve(result.value) : adopt(result.value).then(fulfilled, rejected); }
        step((generator = generator.apply(thisArg, _arguments || [])).next());
    });
};
Object.defineProperty(exports, "__esModule", { value: true });
exports.MainWindow = void 0;
const electron_1 = require("electron");
const MainApp_1 = require("./MainApp");
const MenuBar_1 = require("./MenuBar");
const TrayIcon_1 = require("./TrayIcon");
const path = require("path");
class MainWindow {
    static init() {
        var windowPropertiesObj = MainApp_1.MainApp.electronStoreObj.get(`windowPropertiesObj`);
        if (windowPropertiesObj && Object.hasOwn(windowPropertiesObj, `pageUrl`)) {
            MainWindow.createWindow(windowPropertiesObj);
        }
        else {
            MainWindow.createWindow({
                'pageUrl': MainApp_1.MainApp.applicationUrl,
                'boundsObj': MainWindow.getCenteredWindowBounds(MainWindow.defaultWidthPx, MainWindow.defaultHeightPx),
            });
        }
    }
    static createWindow(windowPropertiesObj) {
        MainWindow.windowObj = new electron_1.BrowserWindow({
            title: MainApp_1.MainApp.packageJsonObj.productName,
            x: windowPropertiesObj.boundsObj.x,
            y: windowPropertiesObj.boundsObj.y,
            width: windowPropertiesObj.boundsObj.width,
            height: windowPropertiesObj.boundsObj.height,
            icon: MainApp_1.MainApp.appIconPath,
            autoHideMenuBar: !MainApp_1.MainApp.electronStoreObj.get(`isAlwaysShowMenuBar`),
            show: !MainApp_1.MainApp.isQuietMode,
            webPreferences: {
                preload: path.join(__dirname, `MainWindowPreload.js`),
                devTools: !MainApp_1.MainApp.isPackaged,
                nodeIntegration: true,
            }
        });
        MainWindow.windowObj.loadURL(windowPropertiesObj.pageUrl || MainApp_1.MainApp.applicationUrl);
        MainWindow.setupWindowEvents();
    }
    static setupWindowEvents() {
        MainWindow.windowObj.on(`close`, function (evt) {
            if (!MainApp_1.MainApp.isQuitting) {
                evt.preventDefault();
                MainWindow.windowObj.hide();
            }
        });
        // @formatter:off
        MainWindow.windowObj.on(`move`, function () { MainWindow.saveWindowProperties(); });
        MainWindow.windowObj.on(`resize`, function () { MainWindow.saveWindowProperties(); });
        MainWindow.windowObj.webContents.on(`did-navigate`, function () { MainWindow.saveWindowProperties(); TrayIcon_1.TrayIcon.setTrayMenu(); });
        MainWindow.windowObj.webContents.on(`did-navigate-in-page`, function () { MainWindow.saveWindowProperties(); TrayIcon_1.TrayIcon.setTrayMenu(); });
        // @formatter:on
        MainWindow.windowObj.on(`focus`, function () {
            MainWindow.windowObj.webContents.send(`focusInput`);
        });
        MainWindow.windowObj.webContents.on(`context-menu`, function () {
            // On Mac (darwin) it doesn`t focus the window if you right-click on it.
            if (MainApp_1.MainApp.platformType == `mac`) {
                MainWindow.windowObj.focus();
            }
            electron_1.Menu.buildFromTemplate(MenuBar_1.MenuBar.generateRightClickMenu()).popup({});
        });
        MainWindow.windowObj.webContents.setWindowOpenHandler(function (detailsObj) {
            if (detailsObj.url.startsWith(MainApp_1.MainApp.applicationUrl)) {
                MainWindow.createWindow({
                    'pageUrl': detailsObj.url,
                    'boundsObj': MainWindow.getCenteredWindowBounds(MainWindow.defaultWidthPx, MainWindow.defaultHeightPx),
                });
            }
            else {
                electron_1.shell.openExternal(detailsObj.url);
            }
            return { action: `deny` };
        });
        MainWindow.windowObj.webContents.on(`will-navigate`, function (evt, navigateUrl) {
            if (!MainWindow.containsAny(navigateUrl, MainApp_1.MainApp.urlContainsAllowlistArr)) {
                electron_1.shell.openExternal(navigateUrl);
                evt.preventDefault();
            }
        });
        MainWindow.windowObj.webContents.ipc.on(`openAIError`, function (evt, lastChatText) {
            if (MainApp_1.MainApp.isPackaged) {
                MainApp_1.MainApp.rollbarObj.error(`OpenAI Error.`);
            }
            MainWindow.lastChatText = lastChatText;
            // Show dialog while reloading.
            const abortControllerObj = new AbortController();
            electron_1.dialog.showMessageBox(MainWindow.windowObj, {
                type: `info`,
                icon: path.join(MainApp_1.MainApp.appPath, `assets`, `icon.png`),
                title: `Reloading...`,
                message: `Reloading... please wait.`,
                detail: `OpenAI Error detected.`,
                signal: abortControllerObj.signal,
            });
            MainWindow.windowObj.reload();
            // Hide dialog once load finishes. Though, it turns out it isn't loading for very long.
            MainWindow.windowObj.webContents.once(`did-stop-loading`, function () {
                abortControllerObj.abort();
            });
        });
        MainWindow.windowObj.webContents.ipc.handle(`getLastChat`, function () {
            return __awaiter(this, void 0, void 0, function* () {
                return MainWindow.lastChatText;
            });
        });
        MainWindow.windowObj.webContents.ipc.on(`lastChatInserted`, function () {
            MainWindow.lastChatText = ``;
        });
    }
    static containsAny(haystackStr, needlesArr) {
        return needlesArr.some((urlContainsStr) => haystackStr.includes(urlContainsStr));
    }
    static getCenteredWindowBounds(widthPx, heightPx) {
        var mainScreenBoundsObj = electron_1.screen.getDisplayNearestPoint(electron_1.screen.getCursorScreenPoint()).bounds;
        var winX = mainScreenBoundsObj.x + ((mainScreenBoundsObj.width - widthPx) / 2);
        var winY = mainScreenBoundsObj.y + ((mainScreenBoundsObj.height - heightPx) / 2);
        return {
            'width': widthPx,
            'height': heightPx,
            'x': Math.round(winX),
            'y': Math.round(winY) // Math.round needed since it throws a strange error without it.
        };
    }
    /*
     * Save all the window's state, for app re-opening.
     * Can't call in `before-quit` because `before-quit` isn't triggered from a Ctrl+C in the terminal.
     */
    static saveWindowProperties() {
        const pageUrl = MainWindow.windowObj.webContents.getURL();
        if (MainWindow.containsAny(pageUrl, MainApp_1.MainApp.dontSaveUrlListArr)) {
            // TODO: Move `pageUrl` out of `windowPropertiesObj` and make `windowPropertiesObj` just store the bounds.
            // Sometimes this causes errors.
            const windowPropertiesObj = MainApp_1.MainApp.electronStoreObj.get(`windowPropertiesObj`);
            MainApp_1.MainApp.electronStoreObj.set(`windowPropertiesObj`, {
                'pageUrl': windowPropertiesObj.pageUrl,
                'boundsObj': MainWindow.windowObj.getBounds(),
            });
        }
        else {
            MainApp_1.MainApp.electronStoreObj.set(`windowPropertiesObj`, {
                'pageUrl': pageUrl,
                'boundsObj': MainWindow.windowObj.getBounds(),
            });
        }
    }
    static showHideMainWin(isShow) {
        if (( // If `isShow` is passed in, use that instead.
        isShow !== undefined
            &&
                isShow)
            ||
                (isShow === undefined
                    &&
                        (!MainWindow.windowObj.isVisible()
                            ||
                                !MainWindow.windowObj.isFocused()))) {
            MainWindow.windowObj.show();
        }
        else {
            MainWindow.windowObj.hide();
        }
    }
}
exports.MainWindow = MainWindow;
MainWindow.defaultWidthPx = 500;
MainWindow.defaultHeightPx = 700;
MainWindow.lastChatText = ``;
//# sourceMappingURL=data:application/json;base64,eyJ2ZXJzaW9uIjozLCJmaWxlIjoiTWFpbldpbmRvdy5qcyIsInNvdXJjZVJvb3QiOiIiLCJzb3VyY2VzIjpbIi4uLy4uL3RzL01haW5XaW5kb3cudHMiXSwibmFtZXMiOltdLCJtYXBwaW5ncyI6Ijs7Ozs7Ozs7Ozs7O0FBQUEsdUNBQW9FO0FBQ3BFLHVDQUFrQztBQUNsQyx1Q0FBa0M7QUFDbEMseUNBQW9DO0FBQ3BDLDZCQUE4QjtBQUU5QixNQUFhLFVBQVU7SUFRWixNQUFNLENBQUMsSUFBSTtRQUVkLElBQUksbUJBQW1CLEdBQUcsaUJBQU8sQ0FBQyxnQkFBZ0IsQ0FBQyxHQUFHLENBQUMscUJBQXFCLENBQUMsQ0FBQztRQUM5RSxJQUFJLG1CQUFtQixJQUFJLE1BQU0sQ0FBQyxNQUFNLENBQUMsbUJBQW1CLEVBQUUsU0FBUyxDQUFDLEVBQUU7WUFDdEUsVUFBVSxDQUFDLFlBQVksQ0FBQyxtQkFBbUIsQ0FBQyxDQUFDO1NBQ2hEO2FBQU07WUFDSCxVQUFVLENBQUMsWUFBWSxDQUFDO2dCQUNwQixTQUFTLEVBQUksaUJBQU8sQ0FBQyxjQUFjO2dCQUNuQyxXQUFXLEVBQUUsVUFBVSxDQUFDLHVCQUF1QixDQUFDLFVBQVUsQ0FBQyxjQUFjLEVBQUUsVUFBVSxDQUFDLGVBQWUsQ0FBQzthQUN6RyxDQUFDLENBQUM7U0FDTjtJQUNMLENBQUM7SUFFTSxNQUFNLENBQUMsWUFBWSxDQUFDLG1CQUFxQztRQUU1RCxVQUFVLENBQUMsU0FBUyxHQUFHLElBQUksd0JBQWEsQ0FBQztZQUNyQyxLQUFLLEVBQVksaUJBQU8sQ0FBQyxjQUFjLENBQUMsV0FBVztZQUNuRCxDQUFDLEVBQWdCLG1CQUFtQixDQUFDLFNBQVMsQ0FBQyxDQUFDO1lBQ2hELENBQUMsRUFBZ0IsbUJBQW1CLENBQUMsU0FBUyxDQUFDLENBQUM7WUFDaEQsS0FBSyxFQUFZLG1CQUFtQixDQUFDLFNBQVMsQ0FBQyxLQUFLO1lBQ3BELE1BQU0sRUFBVyxtQkFBbUIsQ0FBQyxTQUFTLENBQUMsTUFBTTtZQUNyRCxJQUFJLEVBQWEsaUJBQU8sQ0FBQyxXQUFXO1lBQ3BDLGVBQWUsRUFBRSxDQUFDLGlCQUFPLENBQUMsZ0JBQWdCLENBQUMsR0FBRyxDQUFDLHFCQUFxQixDQUFDO1lBQ3JFLElBQUksRUFBYSxDQUFDLGlCQUFPLENBQUMsV0FBVztZQUNyQyxjQUFjLEVBQUc7Z0JBQ2IsT0FBTyxFQUFVLElBQUksQ0FBQyxJQUFJLENBQUMsU0FBUyxFQUFFLHNCQUFzQixDQUFDO2dCQUM3RCxRQUFRLEVBQVMsQ0FBQyxpQkFBTyxDQUFDLFVBQVU7Z0JBQ3BDLGVBQWUsRUFBRSxJQUFJO2FBQ3hCO1NBQ0osQ0FBQyxDQUFDO1FBQ0gsVUFBVSxDQUFDLFNBQVMsQ0FBQyxPQUFPLENBQUMsbUJBQW1CLENBQUMsT0FBTyxJQUFJLGlCQUFPLENBQUMsY0FBYyxDQUFDLENBQUM7UUFDcEYsVUFBVSxDQUFDLGlCQUFpQixFQUFFLENBQUM7SUFDbkMsQ0FBQztJQUVPLE1BQU0sQ0FBQyxpQkFBaUI7UUFFNUIsVUFBVSxDQUFDLFNBQVMsQ0FBQyxFQUFFLENBQUMsT0FBTyxFQUFFLFVBQVUsR0FBRztZQUUxQyxJQUFJLENBQUMsaUJBQU8sQ0FBQyxVQUFVLEVBQUU7Z0JBQ3JCLEdBQUcsQ0FBQyxjQUFjLEVBQUUsQ0FBQztnQkFDckIsVUFBVSxDQUFDLFNBQVMsQ0FBQyxJQUFJLEVBQUUsQ0FBQzthQUMvQjtRQUNMLENBQUMsQ0FBQyxDQUFDO1FBQ0gsaUJBQWlCO1FBQ2pCLFVBQVUsQ0FBQyxTQUFTLENBQUMsRUFBRSxDQUFDLE1BQU0sRUFBOEIsY0FBOEMsVUFBVSxDQUFDLG9CQUFvQixFQUFFLENBQUMsQ0FBeUIsQ0FBQyxDQUFDLENBQUM7UUFDeEssVUFBVSxDQUFDLFNBQVMsQ0FBQyxFQUFFLENBQUMsUUFBUSxFQUE0QixjQUE4QyxVQUFVLENBQUMsb0JBQW9CLEVBQUUsQ0FBQyxDQUF5QixDQUFDLENBQUMsQ0FBQztRQUN4SyxVQUFVLENBQUMsU0FBUyxDQUFDLFdBQVcsQ0FBQyxFQUFFLENBQUMsY0FBYyxFQUFVLGNBQThDLFVBQVUsQ0FBQyxvQkFBb0IsRUFBRSxDQUFDLENBQUMsbUJBQVEsQ0FBQyxXQUFXLEVBQUUsQ0FBQyxDQUFDLENBQUMsQ0FBQyxDQUFDO1FBQ3hLLFVBQVUsQ0FBQyxTQUFTLENBQUMsV0FBVyxDQUFDLEVBQUUsQ0FBQyxzQkFBc0IsRUFBRSxjQUE4QyxVQUFVLENBQUMsb0JBQW9CLEVBQUUsQ0FBQyxDQUFDLG1CQUFRLENBQUMsV0FBVyxFQUFFLENBQUMsQ0FBQyxDQUFDLENBQUMsQ0FBQztRQUN4SyxnQkFBZ0I7UUFDaEIsVUFBVSxDQUFDLFNBQVMsQ0FBQyxFQUFFLENBQUMsT0FBTyxFQUFFO1lBRTdCLFVBQVUsQ0FBQyxTQUFTLENBQUMsV0FBVyxDQUFDLElBQUksQ0FBQyxZQUFZLENBQUMsQ0FBQztRQUN4RCxDQUFDLENBQUMsQ0FBQztRQUNILFVBQVUsQ0FBQyxTQUFTLENBQUMsV0FBVyxDQUFDLEVBQUUsQ0FBQyxjQUFjLEVBQUU7WUFFaEQsd0VBQXdFO1lBQ3hFLElBQUksaUJBQU8sQ0FBQyxZQUFZLElBQUksS0FBSyxFQUFFO2dCQUMvQixVQUFVLENBQUMsU0FBUyxDQUFDLEtBQUssRUFBRSxDQUFDO2FBQ2hDO1lBQ0QsZUFBSSxDQUFDLGlCQUFpQixDQUFDLGlCQUFPLENBQUMsc0JBQXNCLEVBQUUsQ0FBQyxDQUFDLEtBQUssQ0FBQyxFQUFFLENBQUMsQ0FBQztRQUN2RSxDQUFDLENBQUMsQ0FBQztRQUNILFVBQVUsQ0FBQyxTQUFTLENBQUMsV0FBVyxDQUFDLG9CQUFvQixDQUFDLFVBQVUsVUFBVTtZQUV0RSxJQUFJLFVBQVUsQ0FBQyxHQUFHLENBQUMsVUFBVSxDQUFDLGlCQUFPLENBQUMsY0FBYyxDQUFDLEVBQUU7Z0JBQ25ELFVBQVUsQ0FBQyxZQUFZLENBQUM7b0JBQ3BCLFNBQVMsRUFBSSxVQUFVLENBQUMsR0FBRztvQkFDM0IsV0FBVyxFQUFFLFVBQVUsQ0FBQyx1QkFBdUIsQ0FBQyxVQUFVLENBQUMsY0FBYyxFQUFFLFVBQVUsQ0FBQyxlQUFlLENBQUM7aUJBQ3pHLENBQUMsQ0FBQzthQUNOO2lCQUFNO2dCQUNILGdCQUFLLENBQUMsWUFBWSxDQUFDLFVBQVUsQ0FBQyxHQUFHLENBQUMsQ0FBQzthQUN0QztZQUNELE9BQU8sRUFBQyxNQUFNLEVBQUUsTUFBTSxFQUFDLENBQUM7UUFDNUIsQ0FBQyxDQUFDLENBQUM7UUFDSCxVQUFVLENBQUMsU0FBUyxDQUFDLFdBQVcsQ0FBQyxFQUFFLENBQUMsZUFBZSxFQUFFLFVBQVUsR0FBRyxFQUFFLFdBQVc7WUFFM0UsSUFBSSxDQUFDLFVBQVUsQ0FBQyxXQUFXLENBQUMsV0FBVyxFQUFFLGlCQUFPLENBQUMsdUJBQXVCLENBQUMsRUFBRTtnQkFDdkUsZ0JBQUssQ0FBQyxZQUFZLENBQUMsV0FBVyxDQUFDLENBQUM7Z0JBQ2hDLEdBQUcsQ0FBQyxjQUFjLEVBQUUsQ0FBQzthQUN4QjtRQUNMLENBQUMsQ0FBQyxDQUFDO1FBQ0gsVUFBVSxDQUFDLFNBQVMsQ0FBQyxXQUFXLENBQUMsR0FBRyxDQUFDLEVBQUUsQ0FBQyxhQUFhLEVBQUUsVUFBVSxHQUEwQixFQUFFLFlBQW9CO1lBRTdHLElBQUksaUJBQU8sQ0FBQyxVQUFVLEVBQUU7Z0JBQ3BCLGlCQUFPLENBQUMsVUFBVSxDQUFDLEtBQUssQ0FBQyxlQUFlLENBQUMsQ0FBQzthQUM3QztZQUNELFVBQVUsQ0FBQyxZQUFZLEdBQUcsWUFBWSxDQUFDO1lBRXZDLCtCQUErQjtZQUMvQixNQUFNLGtCQUFrQixHQUFHLElBQUksZUFBZSxFQUFFLENBQUM7WUFDakQsaUJBQU0sQ0FBQyxjQUFjLENBQUMsVUFBVSxDQUFDLFNBQVMsRUFBRTtnQkFDeEMsSUFBSSxFQUFLLE1BQU07Z0JBQ2YsSUFBSSxFQUFLLElBQUksQ0FBQyxJQUFJLENBQUMsaUJBQU8sQ0FBQyxPQUFPLEVBQUUsUUFBUSxFQUFFLFVBQVUsQ0FBQztnQkFDekQsS0FBSyxFQUFJLGNBQWM7Z0JBQ3ZCLE9BQU8sRUFBRSwyQkFBMkI7Z0JBQ3BDLE1BQU0sRUFBRyx3QkFBd0I7Z0JBQ2pDLE1BQU0sRUFBRyxrQkFBa0IsQ0FBQyxNQUFNO2FBQ3JDLENBQUMsQ0FBQztZQUNILFVBQVUsQ0FBQyxTQUFTLENBQUMsTUFBTSxFQUFFLENBQUM7WUFDOUIsdUZBQXVGO1lBQ3ZGLFVBQVUsQ0FBQyxTQUFTLENBQUMsV0FBVyxDQUFDLElBQUksQ0FBQyxrQkFBa0IsRUFBRTtnQkFFdEQsa0JBQWtCLENBQUMsS0FBSyxFQUFFLENBQUM7WUFDL0IsQ0FBQyxDQUFDLENBQUM7UUFDUCxDQUFDLENBQUMsQ0FBQztRQUNILFVBQVUsQ0FBQyxTQUFTLENBQUMsV0FBVyxDQUFDLEdBQUcsQ0FBQyxNQUFNLENBQUMsYUFBYSxFQUFFOztnQkFFdkQsT0FBTyxVQUFVLENBQUMsWUFBWSxDQUFDO1lBQ25DLENBQUM7U0FBQSxDQUFDLENBQUM7UUFDSCxVQUFVLENBQUMsU0FBUyxDQUFDLFdBQVcsQ0FBQyxHQUFHLENBQUMsRUFBRSxDQUFDLGtCQUFrQixFQUFFO1lBRXhELFVBQVUsQ0FBQyxZQUFZLEdBQUcsRUFBRSxDQUFDO1FBQ2pDLENBQUMsQ0FBQyxDQUFDO0lBQ1AsQ0FBQztJQUVNLE1BQU0sQ0FBQyxXQUFXLENBQUMsV0FBbUIsRUFBRSxVQUFvQjtRQUUvRCxPQUFPLFVBQVUsQ0FBQyxJQUFJLENBQUMsQ0FBQyxjQUFjLEVBQVcsRUFBRSxDQUFDLFdBQVcsQ0FBQyxRQUFRLENBQUMsY0FBYyxDQUFDLENBQUMsQ0FBQztJQUM5RixDQUFDO0lBRU0sTUFBTSxDQUFDLHVCQUF1QixDQUFDLE9BQWUsRUFBRSxRQUFnQjtRQUVuRSxJQUFJLG1CQUFtQixHQUFHLGlCQUFNLENBQUMsc0JBQXNCLENBQUMsaUJBQU0sQ0FBQyxvQkFBb0IsRUFBRSxDQUFDLENBQUMsTUFBTSxDQUFDO1FBQzlGLElBQUksSUFBSSxHQUFrQixtQkFBbUIsQ0FBQyxDQUFDLEdBQUcsQ0FBQyxDQUFDLG1CQUFtQixDQUFDLEtBQUssR0FBRyxPQUFPLENBQUMsR0FBRyxDQUFDLENBQUMsQ0FBQztRQUM5RixJQUFJLElBQUksR0FBa0IsbUJBQW1CLENBQUMsQ0FBQyxHQUFHLENBQUMsQ0FBQyxtQkFBbUIsQ0FBQyxNQUFNLEdBQUcsUUFBUSxDQUFDLEdBQUcsQ0FBQyxDQUFDLENBQUM7UUFDaEcsT0FBTztZQUNILE9BQU8sRUFBRyxPQUFPO1lBQ2pCLFFBQVEsRUFBRSxRQUFRO1lBQ2xCLEdBQUcsRUFBTyxJQUFJLENBQUMsS0FBSyxDQUFDLElBQUksQ0FBQztZQUMxQixHQUFHLEVBQU8sSUFBSSxDQUFDLEtBQUssQ0FBQyxJQUFJLENBQUMsQ0FBQyxnRUFBZ0U7U0FDOUYsQ0FBQztJQUNOLENBQUM7SUFFRDs7O09BR0c7SUFDSSxNQUFNLENBQUMsb0JBQW9CO1FBRTlCLE1BQU0sT0FBTyxHQUFHLFVBQVUsQ0FBQyxTQUFTLENBQUMsV0FBVyxDQUFDLE1BQU0sRUFBRSxDQUFDO1FBQzFELElBQUksVUFBVSxDQUFDLFdBQVcsQ0FBQyxPQUFPLEVBQUUsaUJBQU8sQ0FBQyxrQkFBa0IsQ0FBQyxFQUFFO1lBQzdELDBHQUEwRztZQUMxRyxnQ0FBZ0M7WUFDaEMsTUFBTSxtQkFBbUIsR0FBRyxpQkFBTyxDQUFDLGdCQUFnQixDQUFDLEdBQUcsQ0FBQyxxQkFBcUIsQ0FBQyxDQUFDO1lBQ2hGLGlCQUFPLENBQUMsZ0JBQWdCLENBQUMsR0FBRyxDQUFDLHFCQUFxQixFQUFFO2dCQUNoRCxTQUFTLEVBQUksbUJBQW1CLENBQUMsT0FBTztnQkFDeEMsV0FBVyxFQUFFLFVBQVUsQ0FBQyxTQUFTLENBQUMsU0FBUyxFQUFFO2FBQ2hELENBQUMsQ0FBQztTQUNOO2FBQU07WUFDSCxpQkFBTyxDQUFDLGdCQUFnQixDQUFDLEdBQUcsQ0FBQyxxQkFBcUIsRUFBRTtnQkFDaEQsU0FBUyxFQUFJLE9BQU87Z0JBQ3BCLFdBQVcsRUFBRSxVQUFVLENBQUMsU0FBUyxDQUFDLFNBQVMsRUFBRTthQUNoRCxDQUFDLENBQUM7U0FDTjtJQUNMLENBQUM7SUFFTSxNQUFNLENBQUMsZUFBZSxDQUFDLE1BQWdCO1FBRTFDLElBQ0ksRUFBRSw4Q0FBOEM7UUFDNUMsTUFBTSxLQUFLLFNBQVM7O2dCQUVwQixNQUFNLENBQ1Q7O2dCQUVELENBQ0ksTUFBTSxLQUFLLFNBQVM7O3dCQUVwQixDQUNJLENBQUMsVUFBVSxDQUFDLFNBQVMsQ0FBQyxTQUFTLEVBQUU7O2dDQUVqQyxDQUFDLFVBQVUsQ0FBQyxTQUFTLENBQUMsU0FBUyxFQUFFLENBQ3BDLENBQ0osRUFDSDtZQUNFLFVBQVUsQ0FBQyxTQUFTLENBQUMsSUFBSSxFQUFFLENBQUM7U0FDL0I7YUFBTTtZQUNILFVBQVUsQ0FBQyxTQUFTLENBQUMsSUFBSSxFQUFFLENBQUM7U0FDL0I7SUFDTCxDQUFDOztBQTFMTCxnQ0E0TEM7QUF4TGlCLHlCQUFjLEdBQUksR0FBRyxDQUFDO0FBQ3RCLDBCQUFlLEdBQUcsR0FBRyxDQUFDO0FBQ3JCLHVCQUFZLEdBQUssRUFBRSxDQUFDIn0=
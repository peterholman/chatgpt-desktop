"use strict";
Object.defineProperty(exports, "__esModule", { value: true });
exports.MenuBar = void 0;
const electron_1 = require("electron");
const MainApp_1 = require("./MainApp");
const MainWindow_1 = require("./MainWindow");
const AboutWindow_1 = require("./AboutWindow");
const path = require("path");
class MenuBar {
    static init() {
        MenuBar.openMenuItemObj = {
            label: `Open ChatGPT`,
            accelerator: MainApp_1.MainApp.shortcutStr_showHide,
            icon: path.join(MainApp_1.MainApp.appPath, `assets`, `menuIcons`, `logo.png`),
            registerAccelerator: false,
            click: function () {
                MainWindow_1.MainWindow.showHideMainWin(true);
            }
        };
        MenuBar.showHideMenuItemObj = {
            label: `Show / hide`,
            accelerator: MainApp_1.MainApp.shortcutStr_showHide,
            icon: path.join(MainApp_1.MainApp.appPath, `assets`, `menuIcons`, `logo.png`),
            registerAccelerator: false,
            click: function () {
                MainWindow_1.MainWindow.showHideMainWin();
            }
        };
        MenuBar.newChatMenuItemObj = {
            label: `New chat`,
            accelerator: MainApp_1.MainApp.shortcutStr_newChat,
            icon: path.join(MainApp_1.MainApp.appPath, `assets`, `menuIcons`, `add.png`),
            registerAccelerator: false,
            click: function () {
                MainApp_1.MainApp.newChat();
            }
        };
        MenuBar.quitMenuItemObj = {
            label: `Quit`,
            click: function () {
                electron_1.app.quit();
            }
        };
        MenuBar.resetAppDataItemObj = {
            label: `Reset app data`,
            click: function () {
                electron_1.dialog.showMessageBox(MainWindow_1.MainWindow.windowObj, {
                    type: `question`,
                    buttons: [`Yes ... reset`, `Cancel`],
                    detail: `All your preferences are permanently wiped, you'll be logged out, and ChatGPT Desktop restarts.`,
                    defaultId: 0,
                    cancelId: 1,
                    message: `Reset app data?`,
                    icon: MainApp_1.MainApp.appIconPath, // On Linux this icon looks blurry on a high density display. Couldn't fix it.
                }).then(function (responseObj) {
                    if (responseObj.response == 0) {
                        MainApp_1.MainApp.electronStoreObj.clear();
                        electron_1.session.defaultSession.clearCache();
                        electron_1.session.defaultSession.clearStorageData();
                        electron_1.app.relaunch();
                        electron_1.app.exit();
                    }
                });
            }
        };
        MenuBar.reportIssueItemObj = {
            label: `Report issue...`,
            click: function () {
                electron_1.shell.openExternal(`https://gitlab.com/joshuaredmond/chatgpt-dekstop/-/issues`);
            }
        };
        MenuBar.homeItemObj = {
            label: `Home`,
            click: function () {
                MainWindow_1.MainWindow.windowObj.loadURL(MainApp_1.MainApp.applicationUrl);
            }
        };
        MenuBar.aboutItemObj = {
            label: `About`,
            click: function () {
                AboutWindow_1.AboutWindow.open();
            }
        };
        MenuBar.runAtStartupItemObj = {
            label: `Run at startup`,
            type: `checkbox`,
            checked: MainApp_1.MainApp.electronStoreObj.get(`isRunAtStartup`),
            visible: MainApp_1.MainApp.isRunAtStartupFeatureEnabled,
            click: function () {
                MainApp_1.MainApp.electronStoreObj.set(`isRunAtStartup`, !MainApp_1.MainApp.electronStoreObj.get(`isRunAtStartup`));
                if (MainApp_1.MainApp.isPackaged) {
                    MainApp_1.MainApp.setRunAtStartup(MainApp_1.MainApp.electronStoreObj.get(`isRunAtStartup`));
                }
            }
        };
        MenuBar.isAlwaysShowMenuBarItemObj = {
            label: `Always show menu bar`,
            type: `checkbox`,
            checked: MainApp_1.MainApp.electronStoreObj.get(`isAlwaysShowMenuBar`),
            visible: [`lin`, `win`].includes(MainApp_1.MainApp.platformType),
            click: function () {
                MainApp_1.MainApp.electronStoreObj.set(`isAlwaysShowMenuBar`, !MainApp_1.MainApp.electronStoreObj.get(`isAlwaysShowMenuBar`));
                MainWindow_1.MainWindow.windowObj.setAutoHideMenuBar(!MainApp_1.MainApp.electronStoreObj.get(`isAlwaysShowMenuBar`));
                MainWindow_1.MainWindow.windowObj.setMenuBarVisibility(MainApp_1.MainApp.electronStoreObj.get(`isAlwaysShowMenuBar`));
            }
        };
        electron_1.Menu.setApplicationMenu(electron_1.Menu.buildFromTemplate(MenuBar.getApplicationMenu()));
    }
    static getTrayIconMenu() {
        return [
            MenuBar.newChatMenuItemObj,
            MenuBar.openMenuItemObj,
            { type: `separator` },
            MenuBar.aboutItemObj,
            MenuBar.reportIssueItemObj,
            MenuBar.resetAppDataItemObj,
            { type: `separator` },
            MenuBar.quitMenuItemObj,
        ];
    }
    static generateRightClickMenu() {
        return [
            { role: `copy`, accelerator: `CmdOrCtrl+C` },
            { role: `paste`, accelerator: `CmdOrCtrl+X` },
        ];
    }
    static getApplicationMenu() {
        var applicationMenuArr = [
            {
                label: `File`,
                submenu: [
                    MenuBar.newChatMenuItemObj,
                    MenuBar.showHideMenuItemObj,
                    { type: `separator` },
                    { role: `quit` },
                ]
            },
            {
                label: `Edit`,
                submenu: [
                    // @formatter:off
                    { label: `Undo`, role: `undo`, accelerator: `CmdOrCtrl+Z` },
                    { label: `Redo`, role: `redo`, accelerator: `CmdOrCtrl+Y` },
                    { type: `separator` },
                    { label: `Cut`, role: `cut`, accelerator: `CmdOrCtrl+X` },
                    { label: `Copy`, role: `copy`, accelerator: `CmdOrCtrl+C` },
                    { label: `Paste`, role: `paste`, accelerator: `CmdOrCtrl+V` },
                    { label: `Paste and match style`, role: `pasteAndMatchStyle`, accelerator: `CmdOrCtrl+Shift+V` },
                    { label: `Select all`, role: `selectAll`, accelerator: `CmdOrCtrl+A` },
                    // @formatter:on
                    // Print doesn't work from an installed snap. Looks hard to get working. See: https://snapcraft.io/docs/cups-interface
                    // {
                    //     label:       `Print`,
                    //     accelerator: `CmdOrCtrl+P`,
                    //     click:       function (item, focusedWindow?: BrowserWindow): void
                    //                  {
                    //                      if (focusedWindow) {
                    //                          focusedWindow.webContents.print({silent: false, printBackground: true});
                    //                      }
                    //                  }
                    // },
                ]
            },
            {
                label: `View`,
                submenu: [
                    { role: `reload` },
                    { role: `forceReload` },
                    { role: `reload`, visible: false, accelerator: `F5` },
                    { type: `separator` },
                    { role: `togglefullscreen` },
                    { type: `separator` },
                    { label: `Zoom in`, role: `zoomIn`, accelerator: `CmdOrCtrl+=` },
                    { label: `Zoom out`, role: `zoomOut`, accelerator: `CmdOrCtrl+-` },
                    { label: `Actual size`, role: `resetZoom`, accelerator: `CmdOrCtrl+0` },
                ]
            },
            {
                label: `History`,
                submenu: [
                    {
                        label: `Forward`,
                        accelerator: `Alt+Right`,
                        click: function (item, focusedWindow) {
                            if (focusedWindow) {
                                focusedWindow.webContents.goForward();
                            }
                        }
                    },
                    {
                        label: `Back`,
                        accelerator: `Alt+Left`,
                        click: function (item, focusedWindow) {
                            if (focusedWindow) {
                                focusedWindow.webContents.goBack();
                            }
                        }
                    }
                ]
            },
            {
                label: `Window`,
                submenu: [
                    MenuBar.runAtStartupItemObj,
                    MenuBar.isAlwaysShowMenuBarItemObj,
                    { type: `separator` },
                    { role: `minimize` },
                    {
                        role: `close`,
                        accelerator: MainApp_1.MainApp.shortcutStr_showHide,
                        registerAccelerator: false,
                    }
                ]
            },
            {
                label: `Help`,
                submenu: [
                    MenuBar.homeItemObj,
                    MenuBar.aboutItemObj,
                    { type: `separator` },
                    MenuBar.reportIssueItemObj,
                    MenuBar.resetAppDataItemObj,
                ]
            },
        ];
        if (!MainApp_1.MainApp.isPackaged && applicationMenuArr[0][`submenu`]) {
            applicationMenuArr[0][`submenu`].push({
                visible: false,
                label: `Open dev tools`,
                accelerator: `F12`,
                role: `toggleDevTools`,
            });
            applicationMenuArr[0][`submenu`].push({
                visible: false,
                label: `Open dev tools`,
                accelerator: `Ctrl+Shift+I`,
                role: `toggleDevTools`,
            });
        }
        return applicationMenuArr;
    }
}
exports.MenuBar = MenuBar;
//# sourceMappingURL=data:application/json;base64,eyJ2ZXJzaW9uIjozLCJmaWxlIjoiTWVudUJhci5qcyIsInNvdXJjZVJvb3QiOiIiLCJzb3VyY2VzIjpbIi4uLy4uL3RzL01lbnVCYXIudHMiXSwibmFtZXMiOltdLCJtYXBwaW5ncyI6Ijs7O0FBQUEsdUNBQTBFO0FBQzFFLHVDQUFrQztBQUNsQyw2Q0FBd0M7QUFDeEMsK0NBQTBDO0FBQzFDLDZCQUE4QjtBQUU5QixNQUFhLE9BQU87SUFjVCxNQUFNLENBQUMsSUFBSTtRQUVkLE9BQU8sQ0FBQyxlQUFlLEdBQWM7WUFDakMsS0FBSyxFQUFnQixjQUFjO1lBQ25DLFdBQVcsRUFBVSxpQkFBTyxDQUFDLG9CQUFvQjtZQUNqRCxJQUFJLEVBQWlCLElBQUksQ0FBQyxJQUFJLENBQUMsaUJBQU8sQ0FBQyxPQUFPLEVBQUUsUUFBUSxFQUFFLFdBQVcsRUFBRSxVQUFVLENBQUM7WUFDbEYsbUJBQW1CLEVBQUUsS0FBSztZQUMxQixLQUFLLEVBQWdCO2dCQUVJLHVCQUFVLENBQUMsZUFBZSxDQUFDLElBQUksQ0FBQyxDQUFDO1lBQ3JDLENBQUM7U0FDekIsQ0FBQztRQUNGLE9BQU8sQ0FBQyxtQkFBbUIsR0FBVTtZQUNqQyxLQUFLLEVBQWdCLGFBQWE7WUFDbEMsV0FBVyxFQUFVLGlCQUFPLENBQUMsb0JBQW9CO1lBQ2pELElBQUksRUFBaUIsSUFBSSxDQUFDLElBQUksQ0FBQyxpQkFBTyxDQUFDLE9BQU8sRUFBRSxRQUFRLEVBQUUsV0FBVyxFQUFFLFVBQVUsQ0FBQztZQUNsRixtQkFBbUIsRUFBRSxLQUFLO1lBQzFCLEtBQUssRUFBZ0I7Z0JBRUksdUJBQVUsQ0FBQyxlQUFlLEVBQUUsQ0FBQztZQUNqQyxDQUFDO1NBQ3pCLENBQUM7UUFDRixPQUFPLENBQUMsa0JBQWtCLEdBQVc7WUFDakMsS0FBSyxFQUFnQixVQUFVO1lBQy9CLFdBQVcsRUFBVSxpQkFBTyxDQUFDLG1CQUFtQjtZQUNoRCxJQUFJLEVBQWlCLElBQUksQ0FBQyxJQUFJLENBQUMsaUJBQU8sQ0FBQyxPQUFPLEVBQUUsUUFBUSxFQUFFLFdBQVcsRUFBRSxTQUFTLENBQUM7WUFDakYsbUJBQW1CLEVBQUUsS0FBSztZQUMxQixLQUFLLEVBQWdCO2dCQUVJLGlCQUFPLENBQUMsT0FBTyxFQUFFLENBQUM7WUFDdEIsQ0FBQztTQUN6QixDQUFDO1FBQ0YsT0FBTyxDQUFDLGVBQWUsR0FBYztZQUNqQyxLQUFLLEVBQUUsTUFBTTtZQUNiLEtBQUssRUFBRTtnQkFFSSxjQUFHLENBQUMsSUFBSSxFQUFFLENBQUM7WUFDZixDQUFDO1NBQ1gsQ0FBQztRQUNGLE9BQU8sQ0FBQyxtQkFBbUIsR0FBVTtZQUNqQyxLQUFLLEVBQUUsZ0JBQWdCO1lBQ3ZCLEtBQUssRUFBRTtnQkFFSyxpQkFBTSxDQUFDLGNBQWMsQ0FDakIsdUJBQVUsQ0FBQyxTQUFTLEVBQ3BCO29CQUNJLElBQUksRUFBTyxVQUFVO29CQUNyQixPQUFPLEVBQUksQ0FBQyxlQUFlLEVBQUUsUUFBUSxDQUFDO29CQUN0QyxNQUFNLEVBQUssaUdBQWlHO29CQUM1RyxTQUFTLEVBQUUsQ0FBQztvQkFDWixRQUFRLEVBQUcsQ0FBQztvQkFDWixPQUFPLEVBQUksaUJBQWlCO29CQUM1QixJQUFJLEVBQU8saUJBQU8sQ0FBQyxXQUFXLEVBQUUsOEVBQThFO2lCQUNqSCxDQUNKLENBQUMsSUFBSSxDQUNGLFVBQVUsV0FBMkM7b0JBRWpELElBQUksV0FBVyxDQUFDLFFBQVEsSUFBSSxDQUFDLEVBQUU7d0JBQzNCLGlCQUFPLENBQUMsZ0JBQWdCLENBQUMsS0FBSyxFQUFFLENBQUM7d0JBRWpDLGtCQUFPLENBQUMsY0FBYyxDQUFDLFVBQVUsRUFBRSxDQUFDO3dCQUNwQyxrQkFBTyxDQUFDLGNBQWMsQ0FBQyxnQkFBZ0IsRUFBRSxDQUFDO3dCQUUxQyxjQUFHLENBQUMsUUFBUSxFQUFFLENBQUM7d0JBQ2YsY0FBRyxDQUFDLElBQUksRUFBRSxDQUFDO3FCQUNkO2dCQUNMLENBQUMsQ0FDSixDQUFDO1lBQ1AsQ0FBQztTQUNYLENBQUM7UUFDRixPQUFPLENBQUMsa0JBQWtCLEdBQVc7WUFDakMsS0FBSyxFQUFFLGlCQUFpQjtZQUN4QixLQUFLLEVBQUU7Z0JBRUksZ0JBQUssQ0FBQyxZQUFZLENBQUMsMkRBQTJELENBQUMsQ0FBQztZQUNwRixDQUFDO1NBQ1gsQ0FBQztRQUNGLE9BQU8sQ0FBQyxXQUFXLEdBQWlCO1lBQ2hDLEtBQUssRUFBRSxNQUFNO1lBQ2IsS0FBSyxFQUFFO2dCQUVJLHVCQUFVLENBQUMsU0FBUyxDQUFDLE9BQU8sQ0FBQyxpQkFBTyxDQUFDLGNBQWMsQ0FBQyxDQUFDO1lBQ3pELENBQUM7U0FDWCxDQUFDO1FBQ0YsT0FBTyxDQUFDLFlBQVksR0FBaUI7WUFDakMsS0FBSyxFQUFFLE9BQU87WUFDZCxLQUFLLEVBQUU7Z0JBRUkseUJBQVcsQ0FBQyxJQUFJLEVBQUUsQ0FBQztZQUN2QixDQUFDO1NBQ1gsQ0FBQztRQUNGLE9BQU8sQ0FBQyxtQkFBbUIsR0FBVTtZQUNqQyxLQUFLLEVBQUksZ0JBQWdCO1lBQ3pCLElBQUksRUFBSyxVQUFVO1lBQ25CLE9BQU8sRUFBRSxpQkFBTyxDQUFDLGdCQUFnQixDQUFDLEdBQUcsQ0FBQyxnQkFBZ0IsQ0FBQztZQUN2RCxPQUFPLEVBQUUsaUJBQU8sQ0FBQyw0QkFBNEI7WUFDN0MsS0FBSyxFQUFJO2dCQUVJLGlCQUFPLENBQUMsZ0JBQWdCLENBQUMsR0FBRyxDQUFDLGdCQUFnQixFQUFFLENBQUMsaUJBQU8sQ0FBQyxnQkFBZ0IsQ0FBQyxHQUFHLENBQUMsZ0JBQWdCLENBQUMsQ0FBQyxDQUFDO2dCQUNoRyxJQUFJLGlCQUFPLENBQUMsVUFBVSxFQUFFO29CQUNwQixpQkFBTyxDQUFDLGVBQWUsQ0FBQyxpQkFBTyxDQUFDLGdCQUFnQixDQUFDLEdBQUcsQ0FBQyxnQkFBZ0IsQ0FBQyxDQUFDLENBQUM7aUJBQzNFO1lBQ0wsQ0FBQztTQUNiLENBQUM7UUFDRixPQUFPLENBQUMsMEJBQTBCLEdBQUc7WUFDakMsS0FBSyxFQUFJLHNCQUFzQjtZQUMvQixJQUFJLEVBQUssVUFBVTtZQUNuQixPQUFPLEVBQUUsaUJBQU8sQ0FBQyxnQkFBZ0IsQ0FBQyxHQUFHLENBQUMscUJBQXFCLENBQUM7WUFDNUQsT0FBTyxFQUFFLENBQUMsS0FBSyxFQUFFLEtBQUssQ0FBQyxDQUFDLFFBQVEsQ0FBQyxpQkFBTyxDQUFDLFlBQVksQ0FBQztZQUN0RCxLQUFLLEVBQUk7Z0JBRUksaUJBQU8sQ0FBQyxnQkFBZ0IsQ0FBQyxHQUFHLENBQUMscUJBQXFCLEVBQUUsQ0FBQyxpQkFBTyxDQUFDLGdCQUFnQixDQUFDLEdBQUcsQ0FBQyxxQkFBcUIsQ0FBQyxDQUFDLENBQUM7Z0JBQzFHLHVCQUFVLENBQUMsU0FBUyxDQUFDLGtCQUFrQixDQUFDLENBQUMsaUJBQU8sQ0FBQyxnQkFBZ0IsQ0FBQyxHQUFHLENBQUMscUJBQXFCLENBQUMsQ0FBQyxDQUFDO2dCQUM5Rix1QkFBVSxDQUFDLFNBQVMsQ0FBQyxvQkFBb0IsQ0FBQyxpQkFBTyxDQUFDLGdCQUFnQixDQUFDLEdBQUcsQ0FBQyxxQkFBcUIsQ0FBQyxDQUFDLENBQUM7WUFDbkcsQ0FBQztTQUNiLENBQUM7UUFDRixlQUFJLENBQUMsa0JBQWtCLENBQUMsZUFBSSxDQUFDLGlCQUFpQixDQUFDLE9BQU8sQ0FBQyxrQkFBa0IsRUFBRSxDQUFDLENBQUMsQ0FBQztJQUNsRixDQUFDO0lBRU0sTUFBTSxDQUFDLGVBQWU7UUFFekIsT0FBTztZQUNILE9BQU8sQ0FBQyxrQkFBa0I7WUFDMUIsT0FBTyxDQUFDLGVBQWU7WUFDdkIsRUFBQyxJQUFJLEVBQUUsV0FBVyxFQUFDO1lBQ25CLE9BQU8sQ0FBQyxZQUFZO1lBQ3BCLE9BQU8sQ0FBQyxrQkFBa0I7WUFDMUIsT0FBTyxDQUFDLG1CQUFtQjtZQUMzQixFQUFDLElBQUksRUFBRSxXQUFXLEVBQUM7WUFDbkIsT0FBTyxDQUFDLGVBQWU7U0FDMUIsQ0FBQztJQUNOLENBQUM7SUFFTSxNQUFNLENBQUMsc0JBQXNCO1FBRWhDLE9BQU87WUFDSCxFQUFDLElBQUksRUFBRSxNQUFNLEVBQUUsV0FBVyxFQUFFLGFBQWEsRUFBQztZQUMxQyxFQUFDLElBQUksRUFBRSxPQUFPLEVBQUUsV0FBVyxFQUFFLGFBQWEsRUFBQztTQUM5QyxDQUFDO0lBQ04sQ0FBQztJQUVNLE1BQU0sQ0FBQyxrQkFBa0I7UUFFNUIsSUFBSSxrQkFBa0IsR0FBMEM7WUFDNUQ7Z0JBQ0ksS0FBSyxFQUFFLE1BQU07Z0JBQ2IsT0FBTyxFQUNBO29CQUNJLE9BQU8sQ0FBQyxrQkFBa0I7b0JBQzFCLE9BQU8sQ0FBQyxtQkFBbUI7b0JBQzNCLEVBQUMsSUFBSSxFQUFFLFdBQVcsRUFBQztvQkFDbkIsRUFBQyxJQUFJLEVBQUUsTUFBTSxFQUFDO2lCQUNqQjthQUNYO1lBQ0Q7Z0JBQ0ksS0FBSyxFQUFJLE1BQU07Z0JBQ2YsT0FBTyxFQUFFO29CQUNMLGlCQUFpQjtvQkFDakIsRUFBQyxLQUFLLEVBQUUsTUFBTSxFQUFzQixJQUFJLEVBQUUsTUFBTSxFQUFvQixXQUFXLEVBQUUsYUFBYSxFQUFDO29CQUMvRixFQUFDLEtBQUssRUFBRSxNQUFNLEVBQXNCLElBQUksRUFBRSxNQUFNLEVBQW9CLFdBQVcsRUFBRSxhQUFhLEVBQUM7b0JBQy9GLEVBQUMsSUFBSSxFQUFFLFdBQVcsRUFBQztvQkFDbkIsRUFBQyxLQUFLLEVBQUUsS0FBSyxFQUF1QixJQUFJLEVBQUUsS0FBSyxFQUFxQixXQUFXLEVBQUUsYUFBYSxFQUFDO29CQUMvRixFQUFDLEtBQUssRUFBRSxNQUFNLEVBQXNCLElBQUksRUFBRSxNQUFNLEVBQW9CLFdBQVcsRUFBRSxhQUFhLEVBQUM7b0JBQy9GLEVBQUMsS0FBSyxFQUFFLE9BQU8sRUFBcUIsSUFBSSxFQUFFLE9BQU8sRUFBbUIsV0FBVyxFQUFFLGFBQWEsRUFBQztvQkFDL0YsRUFBQyxLQUFLLEVBQUUsdUJBQXVCLEVBQUssSUFBSSxFQUFFLG9CQUFvQixFQUFNLFdBQVcsRUFBRSxtQkFBbUIsRUFBQztvQkFDckcsRUFBQyxLQUFLLEVBQUUsWUFBWSxFQUFnQixJQUFJLEVBQUUsV0FBVyxFQUFlLFdBQVcsRUFBRSxhQUFhLEVBQUM7b0JBQy9GLGdCQUFnQjtvQkFFaEIsc0hBQXNIO29CQUN0SCxJQUFJO29CQUNKLDRCQUE0QjtvQkFDNUIsa0NBQWtDO29CQUNsQyx3RUFBd0U7b0JBQ3hFLHFCQUFxQjtvQkFDckIsNENBQTRDO29CQUM1QyxvR0FBb0c7b0JBQ3BHLHlCQUF5QjtvQkFDekIscUJBQXFCO29CQUNyQixLQUFLO2lCQUNSO2FBQ0o7WUFDRDtnQkFDSSxLQUFLLEVBQUksTUFBTTtnQkFDZixPQUFPLEVBQUU7b0JBQ0wsRUFBQyxJQUFJLEVBQUUsUUFBUSxFQUFDO29CQUNoQixFQUFDLElBQUksRUFBRSxhQUFhLEVBQUM7b0JBQ3JCLEVBQUMsSUFBSSxFQUFFLFFBQVEsRUFBRSxPQUFPLEVBQUUsS0FBSyxFQUFFLFdBQVcsRUFBRSxJQUFJLEVBQUM7b0JBQ25ELEVBQUMsSUFBSSxFQUFFLFdBQVcsRUFBQztvQkFDbkIsRUFBQyxJQUFJLEVBQUUsa0JBQWtCLEVBQUM7b0JBQzFCLEVBQUMsSUFBSSxFQUFFLFdBQVcsRUFBQztvQkFDbkIsRUFBQyxLQUFLLEVBQUUsU0FBUyxFQUFFLElBQUksRUFBRSxRQUFRLEVBQUUsV0FBVyxFQUFFLGFBQWEsRUFBQztvQkFDOUQsRUFBQyxLQUFLLEVBQUUsVUFBVSxFQUFFLElBQUksRUFBRSxTQUFTLEVBQUUsV0FBVyxFQUFFLGFBQWEsRUFBQztvQkFDaEUsRUFBQyxLQUFLLEVBQUUsYUFBYSxFQUFFLElBQUksRUFBRSxXQUFXLEVBQUUsV0FBVyxFQUFFLGFBQWEsRUFBQztpQkFDeEU7YUFDSjtZQUNEO2dCQUNJLEtBQUssRUFBSSxTQUFTO2dCQUNsQixPQUFPLEVBQUU7b0JBQ0w7d0JBQ0ksS0FBSyxFQUFRLFNBQVM7d0JBQ3RCLFdBQVcsRUFBRSxXQUFXO3dCQUN4QixLQUFLLEVBQVEsVUFBVSxJQUFJLEVBQUUsYUFBNkI7NEJBRXpDLElBQUksYUFBYSxFQUFFO2dDQUNmLGFBQWEsQ0FBQyxXQUFXLENBQUMsU0FBUyxFQUFFLENBQUM7NkJBQ3pDO3dCQUNMLENBQUM7cUJBQ2pCO29CQUNEO3dCQUNJLEtBQUssRUFBUSxNQUFNO3dCQUNuQixXQUFXLEVBQUUsVUFBVTt3QkFDdkIsS0FBSyxFQUFRLFVBQVUsSUFBSSxFQUFFLGFBQTZCOzRCQUV6QyxJQUFJLGFBQWEsRUFBRTtnQ0FDZixhQUFhLENBQUMsV0FBVyxDQUFDLE1BQU0sRUFBRSxDQUFDOzZCQUN0Qzt3QkFDTCxDQUFDO3FCQUNqQjtpQkFDSjthQUNKO1lBQ0Q7Z0JBQ0ksS0FBSyxFQUFJLFFBQVE7Z0JBQ2pCLE9BQU8sRUFBRTtvQkFDTCxPQUFPLENBQUMsbUJBQW1CO29CQUMzQixPQUFPLENBQUMsMEJBQTBCO29CQUNsQyxFQUFDLElBQUksRUFBRSxXQUFXLEVBQUM7b0JBQ25CLEVBQUMsSUFBSSxFQUFFLFVBQVUsRUFBQztvQkFDbEI7d0JBQ0ksSUFBSSxFQUFpQixPQUFPO3dCQUM1QixXQUFXLEVBQVUsaUJBQU8sQ0FBQyxvQkFBb0I7d0JBQ2pELG1CQUFtQixFQUFFLEtBQUs7cUJBQzdCO2lCQUNKO2FBQ0o7WUFDRDtnQkFDSSxLQUFLLEVBQUksTUFBTTtnQkFDZixPQUFPLEVBQUU7b0JBQ0wsT0FBTyxDQUFDLFdBQVc7b0JBQ25CLE9BQU8sQ0FBQyxZQUFZO29CQUNwQixFQUFDLElBQUksRUFBRSxXQUFXLEVBQUM7b0JBQ25CLE9BQU8sQ0FBQyxrQkFBa0I7b0JBQzFCLE9BQU8sQ0FBQyxtQkFBbUI7aUJBQzlCO2FBQ0o7U0FDSixDQUFDO1FBQ0YsSUFBSSxDQUFDLGlCQUFPLENBQUMsVUFBVSxJQUFJLGtCQUFrQixDQUFDLENBQUMsQ0FBQyxDQUFDLFNBQVMsQ0FBQyxFQUFFO1lBQ2pCLGtCQUFrQixDQUFDLENBQUMsQ0FBQyxDQUFDLFNBQVMsQ0FBRSxDQUFDLElBQUksQ0FDMUU7Z0JBQ0ksT0FBTyxFQUFNLEtBQUs7Z0JBQ2xCLEtBQUssRUFBUSxnQkFBZ0I7Z0JBQzdCLFdBQVcsRUFBRSxLQUFLO2dCQUNsQixJQUFJLEVBQVMsZ0JBQWdCO2FBQ2hDLENBQ0osQ0FBQztZQUNzQyxrQkFBa0IsQ0FBQyxDQUFDLENBQUMsQ0FBQyxTQUFTLENBQUUsQ0FBQyxJQUFJLENBQzFFO2dCQUNJLE9BQU8sRUFBTSxLQUFLO2dCQUNsQixLQUFLLEVBQVEsZ0JBQWdCO2dCQUM3QixXQUFXLEVBQUUsY0FBYztnQkFDM0IsSUFBSSxFQUFTLGdCQUFnQjthQUNoQyxDQUNKLENBQUM7U0FDTDtRQUNELE9BQU8sa0JBQWtCLENBQUM7SUFDOUIsQ0FBQztDQUVKO0FBeFJELDBCQXdSQyJ9
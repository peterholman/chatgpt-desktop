"use strict";
Object.defineProperty(exports, "__esModule", { value: true });
const electron_1 = require("electron");
var $;
let sendLastChatText = ``;
let restoreLastChatText = ``;
// Called when window is shown.
electron_1.ipcRenderer.on(`focusInput`, function () {
    if (window.location.href.startsWith(`https://chat.openai.com`)) {
        document.getElementsByTagName(`textarea`)[0].focus();
    }
});
electron_1.ipcRenderer.on(`newChat`, function () {
    if (window.location.href.startsWith(`https://chat.openai.com`)) {
        if ($(`nav`).length) { // Side menu is popped out.
            $(`nav > a:contains(New chat)`).get(0).click();
        }
        else {
            $(`button`).get(1).click(); // Not a great selector, but it's all I've got.
        }
    }
});
electron_1.ipcRenderer.invoke(`getLastChat`).then(function (lastChatText) {
    if (lastChatText) {
        restoreLastChatText = lastChatText;
    }
});
function addTextareaFocusEvent() {
    $(document).on(`focus`, `textarea:first`, function () {
        if (restoreLastChatText) {
            this.value = restoreLastChatText;
            // Something clears the value afterwards, so keep restoring it until it sticks.
            let triesCount = 0;
            const intervalId = setInterval(function () {
                const $textareaJq = $(`textarea:first`);
                if ($textareaJq.val()) {
                    clearInterval(intervalId);
                    electron_1.ipcRenderer.send(`lastChatInserted`);
                    restoreLastChatText = ``;
                    // Send chat.
                    $textareaJq.next(`button`).trigger(`click`);
                }
                else {
                    $textareaJq.val(restoreLastChatText);
                    triesCount++;
                    if (triesCount > 20) {
                        clearInterval(intervalId);
                    }
                }
            }, 200);
        }
    });
}
function addTextareaKeydownEvent() {
    $(document).on(`keydown`, `textarea:first`, function (evt) {
        if (evt.key == `Enter`) {
            if (evt.ctrlKey || evt.shiftKey) {
                evt.preventDefault();
                evt.stopImmediatePropagation(); // Regular stopPropagation() doesn't work.
                // Add line break.
                this.value = `${this.value}\n`;
                this.dispatchEvent(new Event(`input`, { 'bubbles': true })); // Without this, the textarea doesn't get taller.
            }
            else {
                // Send chat.
                evt.preventDefault();
                // Next element is the button element.
                this.nextElementSibling.click(); // Triggers event below.
            }
        }
    });
}
function addSendButtonClickEvent() {
    // If the user clicks the send button, reload if there's an error.
    $(document).on(`click`, `textarea:first + button`, function () {
        sendLastChatText = $(`textarea:first`).val();
        // Reload if there's an error.
        checkForOpenAIError().then(function (hasError) {
            if (hasError) {
                electron_1.ipcRenderer.send(`openAIError`, sendLastChatText);
            }
        });
    });
}
// To cause an error, delete the `cf_clearance` cookie before sending a message.
function openAIHasError() {
    return Boolean($(`main:contains(An error occurred)`).length) || Boolean($(`main:contains(Something went wrong)`).length);
}
function checkForOpenAIError() {
    if (openAIHasError()) {
        return Promise.resolve(true);
    }
    else {
        return new Promise(function (resolveFunc) {
            let triesCount = 0;
            const intervalId = setInterval(function () {
                if (openAIHasError()) {
                    resolveFunc(true);
                    clearInterval(intervalId);
                }
                else {
                    triesCount++;
                    if (triesCount > 20) {
                        clearInterval(intervalId);
                        resolveFunc(false);
                    }
                }
            }, 200);
        });
    }
}
window.addEventListener(`load`, function () {
    $ = require(`jquery`);
    if (window.location.href.startsWith(`https://chat.openai.com`)) {
        addTextareaFocusEvent();
        addTextareaKeydownEvent();
        addSendButtonClickEvent();
    }
});
//# sourceMappingURL=data:application/json;base64,eyJ2ZXJzaW9uIjozLCJmaWxlIjoiTWFpbldpbmRvd1ByZWxvYWQuanMiLCJzb3VyY2VSb290IjoiIiwic291cmNlcyI6WyIuLi8uLi90cy9NYWluV2luZG93UHJlbG9hZC50cyJdLCJuYW1lcyI6W10sIm1hcHBpbmdzIjoiOztBQUFBLHVDQUFxQztBQUVyQyxJQUFJLENBQU0sQ0FBQztBQUNYLElBQUksZ0JBQWdCLEdBQU0sRUFBRSxDQUFDO0FBQzdCLElBQUksbUJBQW1CLEdBQUcsRUFBRSxDQUFDO0FBRTdCLCtCQUErQjtBQUMvQixzQkFBVyxDQUFDLEVBQUUsQ0FBQyxZQUFZLEVBQUU7SUFFekIsSUFBSSxNQUFNLENBQUMsUUFBUSxDQUFDLElBQUksQ0FBQyxVQUFVLENBQUMseUJBQXlCLENBQUMsRUFBRTtRQUM1RCxRQUFRLENBQUMsb0JBQW9CLENBQUMsVUFBVSxDQUFDLENBQUMsQ0FBQyxDQUFDLENBQUMsS0FBSyxFQUFFLENBQUM7S0FDeEQ7QUFDTCxDQUFDLENBQUMsQ0FBQztBQUVILHNCQUFXLENBQUMsRUFBRSxDQUFDLFNBQVMsRUFBRTtJQUV0QixJQUFJLE1BQU0sQ0FBQyxRQUFRLENBQUMsSUFBSSxDQUFDLFVBQVUsQ0FBQyx5QkFBeUIsQ0FBQyxFQUFFO1FBQzVELElBQUksQ0FBQyxDQUFDLEtBQUssQ0FBQyxDQUFDLE1BQU0sRUFBRSxFQUFFLDJCQUEyQjtZQUM5QyxDQUFDLENBQUMsNEJBQTRCLENBQUMsQ0FBQyxHQUFHLENBQUMsQ0FBQyxDQUFDLENBQUMsS0FBSyxFQUFFLENBQUM7U0FDbEQ7YUFBTTtZQUNILENBQUMsQ0FBQyxRQUFRLENBQUMsQ0FBQyxHQUFHLENBQUMsQ0FBQyxDQUFDLENBQUMsS0FBSyxFQUFFLENBQUMsQ0FBQywrQ0FBK0M7U0FDOUU7S0FDSjtBQUNMLENBQUMsQ0FBQyxDQUFDO0FBRUgsc0JBQVcsQ0FBQyxNQUFNLENBQUMsYUFBYSxDQUFDLENBQUMsSUFBSSxDQUFDLFVBQVUsWUFBb0I7SUFFakUsSUFBSSxZQUFZLEVBQUU7UUFDZCxtQkFBbUIsR0FBRyxZQUFZLENBQUM7S0FDdEM7QUFDTCxDQUFDLENBQUMsQ0FBQztBQUVILFNBQVMscUJBQXFCO0lBRTFCLENBQUMsQ0FBQyxRQUFRLENBQUMsQ0FBQyxFQUFFLENBQUMsT0FBTyxFQUFFLGdCQUFnQixFQUFFO1FBRXRDLElBQUksbUJBQW1CLEVBQUU7WUFDckIsSUFBSSxDQUFDLEtBQUssR0FBRyxtQkFBbUIsQ0FBQztZQUVqQywrRUFBK0U7WUFDL0UsSUFBSSxVQUFVLEdBQUssQ0FBQyxDQUFDO1lBQ3JCLE1BQU0sVUFBVSxHQUFHLFdBQVcsQ0FBQztnQkFFM0IsTUFBTSxXQUFXLEdBQUcsQ0FBQyxDQUFDLGdCQUFnQixDQUFDLENBQUM7Z0JBQ3hDLElBQUksV0FBVyxDQUFDLEdBQUcsRUFBRSxFQUFFO29CQUNuQixhQUFhLENBQUMsVUFBVSxDQUFDLENBQUM7b0JBQzFCLHNCQUFXLENBQUMsSUFBSSxDQUFDLGtCQUFrQixDQUFDLENBQUM7b0JBQ3JDLG1CQUFtQixHQUFHLEVBQUUsQ0FBQztvQkFFekIsYUFBYTtvQkFDYixXQUFXLENBQUMsSUFBSSxDQUFDLFFBQVEsQ0FBQyxDQUFDLE9BQU8sQ0FBQyxPQUFPLENBQUMsQ0FBQztpQkFDL0M7cUJBQU07b0JBQ0gsV0FBVyxDQUFDLEdBQUcsQ0FBQyxtQkFBbUIsQ0FBQyxDQUFDO29CQUNyQyxVQUFVLEVBQUUsQ0FBQztvQkFDYixJQUFJLFVBQVUsR0FBRyxFQUFFLEVBQUU7d0JBQ2pCLGFBQWEsQ0FBQyxVQUFVLENBQUMsQ0FBQztxQkFDN0I7aUJBQ0o7WUFDTCxDQUFDLEVBQUUsR0FBRyxDQUFDLENBQUM7U0FDWDtJQUNMLENBQUMsQ0FBQyxDQUFDO0FBQ1AsQ0FBQztBQUVELFNBQVMsdUJBQXVCO0lBRTVCLENBQUMsQ0FBQyxRQUFRLENBQUMsQ0FBQyxFQUFFLENBQUMsU0FBUyxFQUFFLGdCQUFnQixFQUFFLFVBQXFDLEdBQXlCO1FBRXRHLElBQUksR0FBRyxDQUFDLEdBQUcsSUFBSSxPQUFPLEVBQUU7WUFDcEIsSUFBSSxHQUFHLENBQUMsT0FBTyxJQUFJLEdBQUcsQ0FBQyxRQUFRLEVBQUU7Z0JBQzdCLEdBQUcsQ0FBQyxjQUFjLEVBQUUsQ0FBQztnQkFDckIsR0FBRyxDQUFDLHdCQUF3QixFQUFFLENBQUMsQ0FBQywwQ0FBMEM7Z0JBRTFFLGtCQUFrQjtnQkFDbEIsSUFBSSxDQUFDLEtBQUssR0FBRyxHQUFHLElBQUksQ0FBQyxLQUFLLElBQUksQ0FBQztnQkFDL0IsSUFBSSxDQUFDLGFBQWEsQ0FBQyxJQUFJLEtBQUssQ0FBQyxPQUFPLEVBQUUsRUFBQyxTQUFTLEVBQUUsSUFBSSxFQUFDLENBQUMsQ0FBQyxDQUFDLENBQUMsaURBQWlEO2FBQy9HO2lCQUFNO2dCQUNILGFBQWE7Z0JBQ2IsR0FBRyxDQUFDLGNBQWMsRUFBRSxDQUFDO2dCQUNyQixzQ0FBc0M7Z0JBQ2xCLElBQUksQ0FBQyxrQkFBbUIsQ0FBQyxLQUFLLEVBQUUsQ0FBQyxDQUFDLHdCQUF3QjthQUNqRjtTQUNKO0lBQ0wsQ0FBQyxDQUFDLENBQUM7QUFDUCxDQUFDO0FBRUQsU0FBUyx1QkFBdUI7SUFFNUIsa0VBQWtFO0lBQ2xFLENBQUMsQ0FBQyxRQUFRLENBQUMsQ0FBQyxFQUFFLENBQUMsT0FBTyxFQUFFLHlCQUF5QixFQUFFO1FBRS9DLGdCQUFnQixHQUFHLENBQUMsQ0FBQyxnQkFBZ0IsQ0FBQyxDQUFDLEdBQUcsRUFBRSxDQUFDO1FBRTdDLDhCQUE4QjtRQUM5QixtQkFBbUIsRUFBRSxDQUFDLElBQUksQ0FBQyxVQUFVLFFBQWlCO1lBRWxELElBQUksUUFBUSxFQUFFO2dCQUNWLHNCQUFXLENBQUMsSUFBSSxDQUFDLGFBQWEsRUFBRSxnQkFBZ0IsQ0FBQyxDQUFDO2FBQ3JEO1FBQ0wsQ0FBQyxDQUFDLENBQUM7SUFDUCxDQUFDLENBQUMsQ0FBQztBQUNQLENBQUM7QUFFRCxnRkFBZ0Y7QUFDaEYsU0FBUyxjQUFjO0lBRW5CLE9BQU8sT0FBTyxDQUFDLENBQUMsQ0FBQyxrQ0FBa0MsQ0FBQyxDQUFDLE1BQU0sQ0FBQyxJQUFJLE9BQU8sQ0FBQyxDQUFDLENBQUMscUNBQXFDLENBQUMsQ0FBQyxNQUFNLENBQUMsQ0FBQztBQUM3SCxDQUFDO0FBRUQsU0FBUyxtQkFBbUI7SUFFeEIsSUFBSSxjQUFjLEVBQUUsRUFBRTtRQUNsQixPQUFPLE9BQU8sQ0FBQyxPQUFPLENBQUMsSUFBSSxDQUFDLENBQUM7S0FDaEM7U0FBTTtRQUNILE9BQU8sSUFBSSxPQUFPLENBQUMsVUFBVSxXQUF3QztZQUVqRSxJQUFJLFVBQVUsR0FBSyxDQUFDLENBQUM7WUFDckIsTUFBTSxVQUFVLEdBQUcsV0FBVyxDQUFDO2dCQUUzQixJQUFJLGNBQWMsRUFBRSxFQUFFO29CQUNsQixXQUFXLENBQUMsSUFBSSxDQUFDLENBQUM7b0JBQ2xCLGFBQWEsQ0FBQyxVQUFVLENBQUMsQ0FBQztpQkFDN0I7cUJBQU07b0JBQ0gsVUFBVSxFQUFFLENBQUM7b0JBQ2IsSUFBSSxVQUFVLEdBQUcsRUFBRSxFQUFFO3dCQUNqQixhQUFhLENBQUMsVUFBVSxDQUFDLENBQUM7d0JBQzFCLFdBQVcsQ0FBQyxLQUFLLENBQUMsQ0FBQztxQkFDdEI7aUJBQ0o7WUFDTCxDQUFDLEVBQUUsR0FBRyxDQUFDLENBQUM7UUFDWixDQUFDLENBQUMsQ0FBQztLQUNOO0FBQ0wsQ0FBQztBQUVELE1BQU0sQ0FBQyxnQkFBZ0IsQ0FBQyxNQUFNLEVBQUU7SUFFNUIsQ0FBQyxHQUFHLE9BQU8sQ0FBQyxRQUFRLENBQUMsQ0FBQztJQUN0QixJQUFJLE1BQU0sQ0FBQyxRQUFRLENBQUMsSUFBSSxDQUFDLFVBQVUsQ0FBQyx5QkFBeUIsQ0FBQyxFQUFFO1FBQzVELHFCQUFxQixFQUFFLENBQUM7UUFDeEIsdUJBQXVCLEVBQUUsQ0FBQztRQUMxQix1QkFBdUIsRUFBRSxDQUFDO0tBQzdCO0FBQ0wsQ0FBQyxDQUFDLENBQUMifQ==
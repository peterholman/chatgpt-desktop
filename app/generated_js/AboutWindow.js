"use strict";
var __awaiter = (this && this.__awaiter) || function (thisArg, _arguments, P, generator) {
    function adopt(value) { return value instanceof P ? value : new P(function (resolve) { resolve(value); }); }
    return new (P || (P = Promise))(function (resolve, reject) {
        function fulfilled(value) { try { step(generator.next(value)); } catch (e) { reject(e); } }
        function rejected(value) { try { step(generator["throw"](value)); } catch (e) { reject(e); } }
        function step(result) { result.done ? resolve(result.value) : adopt(result.value).then(fulfilled, rejected); }
        step((generator = generator.apply(thisArg, _arguments || [])).next());
    });
};
var __importDefault = (this && this.__importDefault) || function (mod) {
    return (mod && mod.__esModule) ? mod : { "default": mod };
};
Object.defineProperty(exports, "__esModule", { value: true });
exports.AboutWindow = void 0;
const path_1 = __importDefault(require("path"));
const electron_1 = require("electron");
const MainApp_1 = require("./MainApp");
const MainWindow_1 = require("./MainWindow");
class AboutWindow {
    static init() {
        var isDebug = false;
        var widthPx = (isDebug) ? 1500 : 23 * 16;
        var heightPx = (isDebug) ? 1000 : 40 * 16;
        var boundsObj = MainWindow_1.MainWindow.getCenteredWindowBounds(widthPx, heightPx);
        AboutWindow.aboutWin = new electron_1.BrowserWindow({
            width: boundsObj.width,
            height: boundsObj.height,
            x: boundsObj.x,
            y: boundsObj.y,
            resizable: isDebug,
            minimizable: false,
            icon: MainApp_1.MainApp.appIconPath,
            title: `About ChatGPT Desktop`,
            backgroundColor: `rgb(249, 249, 249)`,
            webPreferences: {
                devTools: isDebug,
                preload: path_1.default.join(__dirname, `AboutWindowPreload.js`),
            },
        });
        AboutWindow.aboutWin.removeMenu();
        AboutWindow.aboutWin.webContents.ipc.handle(`versionStr`, function () {
            return __awaiter(this, void 0, void 0, function* () {
                return MainApp_1.MainApp.packageJsonObj.version;
            });
        });
        AboutWindow.aboutWin.webContents.ipc.on(`closeAboutWin`, function () {
            var _a;
            (_a = AboutWindow.aboutWin) === null || _a === void 0 ? void 0 : _a.close();
        });
        AboutWindow.aboutWin.on(`closed`, function () {
            AboutWindow.aboutWin = null;
        });
        if (isDebug) {
            AboutWindow.aboutWin.on(`resize`, function () {
                var _a;
                console.log((_a = AboutWindow.aboutWin) === null || _a === void 0 ? void 0 : _a.getSize()); // Allow `console.log`.
            });
        }
        AboutWindow.aboutWin.webContents.on(`will-navigate`, function (evt, navigateUrl) {
            evt.preventDefault();
            electron_1.shell.openExternal(navigateUrl);
        });
        AboutWindow.aboutWin.loadURL(`file://${__dirname}/../aboutWindow.html`);
    }
    static open() {
        if (AboutWindow.aboutWin) {
            AboutWindow.aboutWin.show();
        }
        else {
            AboutWindow.init();
        }
    }
}
exports.AboutWindow = AboutWindow;
//# sourceMappingURL=data:application/json;base64,eyJ2ZXJzaW9uIjozLCJmaWxlIjoiQWJvdXRXaW5kb3cuanMiLCJzb3VyY2VSb290IjoiIiwic291cmNlcyI6WyIuLi8uLi90cy9BYm91dFdpbmRvdy50cyJdLCJuYW1lcyI6W10sIm1hcHBpbmdzIjoiOzs7Ozs7Ozs7Ozs7Ozs7QUFBQSxnREFBd0I7QUFDeEIsdUNBQThDO0FBQzlDLHVDQUFrQztBQUNsQyw2Q0FBd0M7QUFFeEMsTUFBYSxXQUFXO0lBS1osTUFBTSxDQUFDLElBQUk7UUFFZixJQUFJLE9BQU8sR0FBWSxLQUFLLENBQUM7UUFDN0IsSUFBSSxPQUFPLEdBQVksQ0FBQyxPQUFPLENBQUMsQ0FBQyxDQUFDLENBQUMsSUFBSSxDQUFDLENBQUMsQ0FBQyxFQUFFLEdBQUcsRUFBRSxDQUFDO1FBQ2xELElBQUksUUFBUSxHQUFXLENBQUMsT0FBTyxDQUFDLENBQUMsQ0FBQyxDQUFDLElBQUksQ0FBQyxDQUFDLENBQUMsRUFBRSxHQUFHLEVBQUUsQ0FBQztRQUNsRCxJQUFJLFNBQVMsR0FBVSx1QkFBVSxDQUFDLHVCQUF1QixDQUFDLE9BQU8sRUFBRSxRQUFRLENBQUMsQ0FBQztRQUM3RSxXQUFXLENBQUMsUUFBUSxHQUFHLElBQUksd0JBQWEsQ0FBQztZQUNyQyxLQUFLLEVBQVksU0FBUyxDQUFDLEtBQUs7WUFDaEMsTUFBTSxFQUFXLFNBQVMsQ0FBQyxNQUFNO1lBQ2pDLENBQUMsRUFBZ0IsU0FBUyxDQUFDLENBQUM7WUFDNUIsQ0FBQyxFQUFnQixTQUFTLENBQUMsQ0FBQztZQUM1QixTQUFTLEVBQVEsT0FBTztZQUN4QixXQUFXLEVBQU0sS0FBSztZQUN0QixJQUFJLEVBQWEsaUJBQU8sQ0FBQyxXQUFXO1lBQ3BDLEtBQUssRUFBWSx1QkFBdUI7WUFDeEMsZUFBZSxFQUFFLG9CQUFvQjtZQUNyQyxjQUFjLEVBQUc7Z0JBQ2IsUUFBUSxFQUFFLE9BQU87Z0JBQ2pCLE9BQU8sRUFBRyxjQUFJLENBQUMsSUFBSSxDQUFDLFNBQVMsRUFBRSx1QkFBdUIsQ0FBQzthQUMxRDtTQUNKLENBQUMsQ0FBQztRQUNILFdBQVcsQ0FBQyxRQUFRLENBQUMsVUFBVSxFQUFFLENBQUM7UUFDbEMsV0FBVyxDQUFDLFFBQVEsQ0FBQyxXQUFXLENBQUMsR0FBRyxDQUFDLE1BQU0sQ0FBQyxZQUFZLEVBQUU7O2dCQUV0RCxPQUFPLGlCQUFPLENBQUMsY0FBYyxDQUFDLE9BQU8sQ0FBQztZQUMxQyxDQUFDO1NBQUEsQ0FBQyxDQUFDO1FBQ0gsV0FBVyxDQUFDLFFBQVEsQ0FBQyxXQUFXLENBQUMsR0FBRyxDQUFDLEVBQUUsQ0FBQyxlQUFlLEVBQUU7O1lBRXJELE1BQUEsV0FBVyxDQUFDLFFBQVEsMENBQUUsS0FBSyxFQUFFLENBQUM7UUFDbEMsQ0FBQyxDQUFDLENBQUM7UUFDSCxXQUFXLENBQUMsUUFBUSxDQUFDLEVBQUUsQ0FBQyxRQUFRLEVBQUU7WUFFOUIsV0FBVyxDQUFDLFFBQVEsR0FBRyxJQUFJLENBQUM7UUFDaEMsQ0FBQyxDQUFDLENBQUM7UUFFSCxJQUFJLE9BQU8sRUFBRTtZQUNULFdBQVcsQ0FBQyxRQUFRLENBQUMsRUFBRSxDQUFDLFFBQVEsRUFBRTs7Z0JBRTlCLE9BQU8sQ0FBQyxHQUFHLENBQUMsTUFBQSxXQUFXLENBQUMsUUFBUSwwQ0FBRSxPQUFPLEVBQUUsQ0FBQyxDQUFDLENBQUMsdUJBQXVCO1lBQ3pFLENBQUMsQ0FBQyxDQUFDO1NBQ047UUFDRCxXQUFXLENBQUMsUUFBUSxDQUFDLFdBQVcsQ0FBQyxFQUFFLENBQUMsZUFBZSxFQUFFLFVBQVUsR0FBbUIsRUFBRSxXQUFtQjtZQUVuRyxHQUFHLENBQUMsY0FBYyxFQUFFLENBQUM7WUFDckIsZ0JBQUssQ0FBQyxZQUFZLENBQUMsV0FBVyxDQUFDLENBQUM7UUFDcEMsQ0FBQyxDQUFDLENBQUM7UUFDSCxXQUFXLENBQUMsUUFBUSxDQUFDLE9BQU8sQ0FBQyxVQUFVLFNBQVMsc0JBQXNCLENBQUMsQ0FBQztJQUM1RSxDQUFDO0lBRU0sTUFBTSxDQUFDLElBQUk7UUFFZCxJQUFJLFdBQVcsQ0FBQyxRQUFRLEVBQUU7WUFDdEIsV0FBVyxDQUFDLFFBQVEsQ0FBQyxJQUFJLEVBQUUsQ0FBQztTQUMvQjthQUFNO1lBQ0gsV0FBVyxDQUFDLElBQUksRUFBRSxDQUFDO1NBQ3RCO0lBQ0wsQ0FBQztDQUVKO0FBL0RELGtDQStEQyJ9
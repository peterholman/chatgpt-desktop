"use strict";
Object.defineProperty(exports, "__esModule", { value: true });
const { ipcRenderer, contextBridge } = require(`electron`);
contextBridge.exposeInMainWorld(`electronIpcRenderer`, {
    onMessageReceived: (messageCode, listenerFunc) => ipcRenderer.on(messageCode, listenerFunc),
    sendMessage: ipcRenderer.send,
    invokeMessage: ipcRenderer.invoke,
});
//# sourceMappingURL=data:application/json;base64,eyJ2ZXJzaW9uIjozLCJmaWxlIjoiQWJvdXRXaW5kb3dQcmVsb2FkLmpzIiwic291cmNlUm9vdCI6IiIsInNvdXJjZXMiOlsiLi4vLi4vdHMvQWJvdXRXaW5kb3dQcmVsb2FkLnRzIl0sIm5hbWVzIjpbXSwibWFwcGluZ3MiOiI7O0FBQUEsTUFBTSxFQUFDLFdBQVcsRUFBRSxhQUFhLEVBQUMsR0FBRyxPQUFPLENBQUMsVUFBVSxDQUFDLENBQUM7QUFFekQsYUFBYSxDQUFDLGlCQUFpQixDQUFDLHFCQUFxQixFQUFFO0lBQ25ELGlCQUFpQixFQUFFLENBQUMsV0FBbUIsRUFBRSxZQUFxRCxFQUFPLEVBQUUsQ0FBQyxXQUFXLENBQUMsRUFBRSxDQUFDLFdBQVcsRUFBRSxZQUFZLENBQUM7SUFDakosV0FBVyxFQUFRLFdBQVcsQ0FBQyxJQUFJO0lBQ25DLGFBQWEsRUFBTSxXQUFXLENBQUMsTUFBTTtDQUN4QyxDQUFDLENBQUMifQ==
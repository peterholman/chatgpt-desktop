"use strict";
var __importDefault = (this && this.__importDefault) || function (mod) {
    return (mod && mod.__esModule) ? mod : { "default": mod };
};
Object.defineProperty(exports, "__esModule", { value: true });
exports.MainApp = void 0;
require("source-map-support/register.js"); // This is makes error messages use the .ts file path instead. Very handy.
const fs_extra_1 = __importDefault(require("fs-extra"));
const path_1 = __importDefault(require("path"));
const electron_store_1 = __importDefault(require("electron-store"));
const rollbar_1 = __importDefault(require("rollbar"));
const electron_1 = require("electron");
const MainWindow_1 = require("./MainWindow");
const MenuBar_1 = require("./MenuBar");
const TrayIcon_1 = require("./TrayIcon");
// @ts-ignore because this file is outside the ts_build folder.
// Allow eslint-disable because eslint hasn't caught up to the new assert syntax.
// eslint-disable-next-line @typescript-eslint/quotes
const package_json_1 = __importDefault(require("../../package.json"));
class MainApp {
    static init() {
        MainApp.initErrorReporting();
        MainApp.setVars();
        MainApp.setRunAtStartup(MainApp.electronStoreObj.get(`isRunAtStartup`));
        MainApp.setupAppEvents();
    }
    static setupAppEvents() {
        electron_1.app.on(`ready`, function () {
            MainApp.onReady();
        });
        electron_1.app.on(`window-all-closed`, function () {
            // Don't quit app.
        });
        electron_1.app.on(`before-quit`, function () {
            MainApp.beforeQuit();
        });
        MainApp.logMessageOnConsoleMessage();
    }
    static logMessageOnConsoleMessage() {
        if (!MainApp.isPackaged) {
            electron_1.app.on(`web-contents-created`, function (evt, webContentsObj) {
                webContentsObj.on(`console-message`, function (evt, levelNum, messageStr) {
                    if (!messageStr.includes(`Electron Security Warning (Insecure Content-Security-Policy)`)) {
                        var levelStr = ``;
                        if (levelNum == 0) {
                            levelStr = `verbose`;
                        }
                        else if (levelNum == 1) {
                            levelStr = `info`;
                        }
                        else if (levelNum == 2) {
                            levelStr = `warning`;
                        }
                        else if (levelNum == 3) {
                            levelStr = `error`;
                        }
                        console.log(`${webContentsObj.getTitle()} ${levelStr}: ${messageStr.replaceAll(`\n`, `\n    `)}`); // Keep for debugging.
                    }
                });
            });
        }
    }
    static initErrorReporting() {
        MainApp.isPackaged = electron_1.app.isPackaged;
        MainApp.packageJsonObj = package_json_1.default;
        // Error reporting.
        if (MainApp.isPackaged) {
            MainApp.rollbarObj = new rollbar_1.default({
                accessToken: `69a1a11139584a4da95b4b111c5c9ca3`,
                captureUncaught: true,
                captureUnhandledRejections: true,
                codeVersion: MainApp.packageJsonObj.version,
                environment: (MainApp.isPackaged) ? `production` : `development`, // Will always be `production`, except when I enable it on dev for testing.
            });
        }
    }
    static setVars() {
        if (process.platform == `win32`) {
            MainApp.platformType = `win`;
            MainApp.iconExtStr = `ico`;
        }
        else if (process.platform == `darwin`) {
            MainApp.platformType = `mac`;
            MainApp.iconExtStr = `icns`;
        }
        else {
            MainApp.platformType = `lin`;
            MainApp.iconExtStr = `png`;
        }
        MainApp.appPath = path_1.default.join(__dirname, `..`);
        MainApp.appIconPath = path_1.default.join(MainApp.appPath, `assets`, `icon.${MainApp.iconExtStr}`);
        MainApp.electronStoreObj = new electron_store_1.default({
            'name': `chatGPTConfig`,
            'clearInvalidConfig': true,
            'cwd': process.env.SNAP_USER_COMMON || electron_1.app.getPath(`userData`),
            'defaults': {
                'isRunAtStartup': false,
                'isAlwaysShowMenuBar': true,
                'windowPropertiesObj': {},
            }
        });
        MainApp.isQuietMode = (Boolean(process.argv.includes(`--hidden`) || process.env.DESKTOP_AUTOSTART_ID)); // On Linux, when installed from snap or deb, the `DESKTOP_AUTOSTART_ID` environment variable will exist when run at startup.
    }
    static onReady() {
        MainApp.setupSingleInstanceLock();
        MenuBar_1.MenuBar.init();
        MainWindow_1.MainWindow.init();
        TrayIcon_1.TrayIcon.init();
        MainApp.registerShortcuts();
    }
    static beforeQuit() {
        MainApp.isQuitting = true;
        MainWindow_1.MainWindow.saveWindowProperties();
    }
    static setRunAtStartup(isRunAtStartup) {
        if (MainApp.isPackaged) {
            if (MainApp.platformType == `lin`) {
                MainApp.setRunAtStartupLin(isRunAtStartup);
            }
            // Windows run at startup isn't built yet. See: `mod/electron/desktop/!Readme - Windows.md:33` (Look for "Run at startup". To open: [_Alt_]+[_Shift_]+[_N_])
        }
    }
    static setRunAtStartupLin(isRunAtStartup) {
        var autostartDesktopFilePath = `${process.env.SNAP_USER_DATA || process.env.HOME}/.config/autostart/${MainApp.packageJsonObj.name}.desktop`; // For some reason `os.userInfo().homedir` doesn't get the snap's home folder when installed from snap.
        var originalDesktopFilePath = ``;
        if (MainApp.electronStoreObj.get(`desktopFilePath`)) {
            originalDesktopFilePath = MainApp.electronStoreObj.get(`desktopFilePath`);
        }
        else {
            if (process.env.BAMF_DESKTOP_FILE_HINT) { // Snap sets this environment variable when the app is run from a .desktop file.
                // For .snap install.
                originalDesktopFilePath = process.env.BAMF_DESKTOP_FILE_HINT;
            }
            else {
                // For .deb install.
                originalDesktopFilePath = `/usr/share/applications/${MainApp.packageJsonObj.name}.desktop`;
            }
            MainApp.electronStoreObj.set(`desktopFilePath`, originalDesktopFilePath);
        }
        fs_extra_1.default.pathExists(originalDesktopFilePath, function (errorObj, isOriginalDesktopFileExists) {
            if (errorObj) {
                MainApp.electronStoreObj.set(`isRunAtStartup`, false);
                throw errorObj;
            }
            else if (isOriginalDesktopFileExists) {
                fs_extra_1.default.pathExists(autostartDesktopFilePath, function (errorObj, isAutostartDesktopFileExists) {
                    if (errorObj) {
                        MainApp.electronStoreObj.set(`isRunAtStartup`, false);
                        throw errorObj;
                    }
                    else {
                        MainApp.isRunAtStartupFeatureEnabled = true;
                        if (isRunAtStartup) {
                            if (!isAutostartDesktopFileExists) {
                                fs_extra_1.default.ensureSymlink(originalDesktopFilePath, autostartDesktopFilePath, function (errorObj) {
                                    if (errorObj) {
                                        MainApp.electronStoreObj.set(`isRunAtStartup`, false); // Reset the setting so they can try again.
                                        throw errorObj;
                                    }
                                });
                            }
                        }
                        else {
                            if (isAutostartDesktopFileExists) {
                                fs_extra_1.default.unlink(autostartDesktopFilePath, function (errorObj) {
                                    if (errorObj) {
                                        MainApp.electronStoreObj.set(`isRunAtStartup`, true); // Reset the setting so they can try again.
                                        throw errorObj;
                                    }
                                });
                            }
                        }
                    }
                });
            }
        });
    }
    /*
     * When a second instance is opened quit the second instance and focus the first instance.
     */
    static setupSingleInstanceLock() {
        var isOnlyInstance = electron_1.app.requestSingleInstanceLock();
        if (isOnlyInstance) {
            electron_1.app.on(`second-instance`, function () {
                MainWindow_1.MainWindow.showHideMainWin(true);
            });
        }
        else {
            electron_1.app.quit();
        }
    }
    static registerShortcuts() {
        MainApp.registerGlobalShortcut(MainApp.shortcutStr_showHide, function () {
            MainWindow_1.MainWindow.showHideMainWin();
        });
        MainApp.registerGlobalShortcut(MainApp.shortcutStr_newChat, function () {
            MainApp.newChat();
        });
    }
    static newChat() {
        MainWindow_1.MainWindow.showHideMainWin(true);
        MainWindow_1.MainWindow.windowObj.webContents.send(`newChat`);
    }
    static registerGlobalShortcut(shortcutKeyStr, callbackFunc) {
        if (electron_1.globalShortcut.register(shortcutKeyStr, callbackFunc)) {
            // Registered sucessfully, do nothing.
        }
        else {
            if (MainApp.isPackaged) {
                MainApp.rollbarObj.error(`Failed to register shortcut: ${shortcutKeyStr}`);
            }
            electron_1.dialog.showMessageBox(MainWindow_1.MainWindow.windowObj, {
                type: `info`,
                icon: path_1.default.join(MainApp.appPath, `assets`, `icon.png`),
                title: `Shortcut taken`,
                message: `Couldn't register shortcut: ${shortcutKeyStr}`,
                detail: `Another app is already using it.`,
                buttons: [`That's ok`, `Report issue...`],
                defaultId: 0,
            }).then(function (responseObj) {
                if (responseObj.response === 1) {
                    if (MainApp.isPackaged) {
                        MainApp.rollbarObj.error(`Issue reported for failed shortcut registration: ${shortcutKeyStr}`);
                    }
                    electron_1.shell.openExternal(`https://gitlab.com/joshuaredmond/chatgpt-dekstop/-/issues`);
                }
            });
        }
    }
}
exports.MainApp = MainApp;
MainApp.shortcutStr_showHide = `Super+Alt+A`;
MainApp.shortcutStr_newChat = `Super+Alt+C`;
MainApp.isQuietMode = false;
MainApp.isQuitting = false;
MainApp.applicationUrl = `https://chat.openai.com/`;
MainApp.urlContainsAllowlistArr = [
    MainApp.applicationUrl,
    `openai.com`,
    `https://auth0.openai.com/`,
    `https://auth.openai.com/`,
    `https://platform.openai.com/`,
    `https://accounts.google.com/`,
    `https://login.live.com/`,
    `https://github.com/`, // GitHub login.
];
MainApp.dontSaveUrlListArr = [
    `https://chat.openai.com/auth/error`,
    `https://auth0.openai.com/`,
    `https://auth.openai.com/`,
    `https://accounts.google.com/`,
    `https://login.live.com/`,
    `https://github.com/`,
];
MainApp.init();
//# sourceMappingURL=data:application/json;base64,eyJ2ZXJzaW9uIjozLCJmaWxlIjoiTWFpbkFwcC5qcyIsInNvdXJjZVJvb3QiOiIiLCJzb3VyY2VzIjpbIi4uLy4uL3RzL01haW5BcHAudHMiXSwibmFtZXMiOltdLCJtYXBwaW5ncyI6Ijs7Ozs7O0FBQUEsMENBQXdDLENBQUMsMEVBQTBFO0FBRW5ILHdEQUEwQjtBQUMxQixnREFBd0I7QUFDeEIsb0VBQTJDO0FBQzNDLHNEQUE4QjtBQUM5Qix1Q0FBNEQ7QUFDNUQsNkNBQXdDO0FBQ3hDLHVDQUFrQztBQUNsQyx5Q0FBb0M7QUFDcEMsK0RBQStEO0FBQy9ELGlGQUFpRjtBQUNqRixxREFBcUQ7QUFDckQsc0VBQXNFO0FBRXRFLE1BQWEsT0FBTztJQXdDVCxNQUFNLENBQUMsSUFBSTtRQUVkLE9BQU8sQ0FBQyxrQkFBa0IsRUFBRSxDQUFDO1FBQzdCLE9BQU8sQ0FBQyxPQUFPLEVBQUUsQ0FBQztRQUNsQixPQUFPLENBQUMsZUFBZSxDQUFDLE9BQU8sQ0FBQyxnQkFBZ0IsQ0FBQyxHQUFHLENBQUMsZ0JBQWdCLENBQUMsQ0FBQyxDQUFDO1FBQ3hFLE9BQU8sQ0FBQyxjQUFjLEVBQUUsQ0FBQztJQUM3QixDQUFDO0lBRU0sTUFBTSxDQUFDLGNBQWM7UUFFeEIsY0FBRyxDQUFDLEVBQUUsQ0FBQyxPQUFPLEVBQUU7WUFFWixPQUFPLENBQUMsT0FBTyxFQUFFLENBQUM7UUFDdEIsQ0FBQyxDQUFDLENBQUM7UUFDSCxjQUFHLENBQUMsRUFBRSxDQUFDLG1CQUFtQixFQUFFO1lBRXhCLGtCQUFrQjtRQUN0QixDQUFDLENBQUMsQ0FBQztRQUNILGNBQUcsQ0FBQyxFQUFFLENBQUMsYUFBYSxFQUFFO1lBRWxCLE9BQU8sQ0FBQyxVQUFVLEVBQUUsQ0FBQztRQUN6QixDQUFDLENBQUMsQ0FBQztRQUNILE9BQU8sQ0FBQywwQkFBMEIsRUFBRSxDQUFDO0lBQ3pDLENBQUM7SUFFTyxNQUFNLENBQUMsMEJBQTBCO1FBRXJDLElBQUksQ0FBQyxPQUFPLENBQUMsVUFBVSxFQUFFO1lBQ3JCLGNBQUcsQ0FBQyxFQUFFLENBQUMsc0JBQXNCLEVBQUUsVUFBVSxHQUFtQixFQUFFLGNBQW9DO2dCQUU5RixjQUFjLENBQUMsRUFBRSxDQUFDLGlCQUFpQixFQUFFLFVBQVUsR0FBbUIsRUFBRSxRQUFnQixFQUFFLFVBQWtCO29CQUVwRyxJQUFJLENBQUMsVUFBVSxDQUFDLFFBQVEsQ0FBQyw4REFBOEQsQ0FBQyxFQUFFO3dCQUN0RixJQUFJLFFBQVEsR0FBRyxFQUFFLENBQUM7d0JBQ2xCLElBQUksUUFBUSxJQUFJLENBQUMsRUFBRTs0QkFDZixRQUFRLEdBQUcsU0FBUyxDQUFDO3lCQUN4Qjs2QkFBTSxJQUFJLFFBQVEsSUFBSSxDQUFDLEVBQUU7NEJBQ3RCLFFBQVEsR0FBRyxNQUFNLENBQUM7eUJBQ3JCOzZCQUFNLElBQUksUUFBUSxJQUFJLENBQUMsRUFBRTs0QkFDdEIsUUFBUSxHQUFHLFNBQVMsQ0FBQzt5QkFDeEI7NkJBQU0sSUFBSSxRQUFRLElBQUksQ0FBQyxFQUFFOzRCQUN0QixRQUFRLEdBQUcsT0FBTyxDQUFDO3lCQUN0Qjt3QkFDRCxPQUFPLENBQUMsR0FBRyxDQUFDLEdBQUcsY0FBYyxDQUFDLFFBQVEsRUFBRSxJQUFJLFFBQVEsS0FBSyxVQUFVLENBQUMsVUFBVSxDQUFDLElBQUksRUFBRSxRQUFRLENBQUMsRUFBRSxDQUFDLENBQUMsQ0FBQyxzQkFBc0I7cUJBQzVIO2dCQUNMLENBQUMsQ0FBQyxDQUFDO1lBQ1AsQ0FBQyxDQUFDLENBQUM7U0FDTjtJQUNMLENBQUM7SUFFTyxNQUFNLENBQUMsa0JBQWtCO1FBRTdCLE9BQU8sQ0FBQyxVQUFVLEdBQU8sY0FBRyxDQUFDLFVBQVUsQ0FBQztRQUN4QyxPQUFPLENBQUMsY0FBYyxHQUFHLHNCQUFjLENBQUM7UUFFeEMsbUJBQW1CO1FBQ25CLElBQUksT0FBTyxDQUFDLFVBQVUsRUFBRTtZQUNwQixPQUFPLENBQUMsVUFBVSxHQUFHLElBQUksaUJBQU8sQ0FBQztnQkFDN0IsV0FBVyxFQUFpQixrQ0FBa0M7Z0JBQzlELGVBQWUsRUFBYSxJQUFJO2dCQUNoQywwQkFBMEIsRUFBRSxJQUFJO2dCQUNoQyxXQUFXLEVBQWlCLE9BQU8sQ0FBQyxjQUFjLENBQUMsT0FBTztnQkFDMUQsV0FBVyxFQUFpQixDQUFDLE9BQU8sQ0FBQyxVQUFVLENBQUMsQ0FBQyxDQUFDLENBQUMsWUFBWSxDQUFDLENBQUMsQ0FBQyxhQUFhLEVBQUUsMkVBQTJFO2FBQy9KLENBQUMsQ0FBQztTQUNOO0lBQ0wsQ0FBQztJQUVPLE1BQU0sQ0FBQyxPQUFPO1FBRWxCLElBQUksT0FBTyxDQUFDLFFBQVEsSUFBSSxPQUFPLEVBQUU7WUFDN0IsT0FBTyxDQUFDLFlBQVksR0FBRyxLQUFLLENBQUM7WUFDN0IsT0FBTyxDQUFDLFVBQVUsR0FBSyxLQUFLLENBQUM7U0FDaEM7YUFBTSxJQUFJLE9BQU8sQ0FBQyxRQUFRLElBQUksUUFBUSxFQUFFO1lBQ3JDLE9BQU8sQ0FBQyxZQUFZLEdBQUcsS0FBSyxDQUFDO1lBQzdCLE9BQU8sQ0FBQyxVQUFVLEdBQUssTUFBTSxDQUFDO1NBQ2pDO2FBQU07WUFDSCxPQUFPLENBQUMsWUFBWSxHQUFHLEtBQUssQ0FBQztZQUM3QixPQUFPLENBQUMsVUFBVSxHQUFLLEtBQUssQ0FBQztTQUNoQztRQUNELE9BQU8sQ0FBQyxPQUFPLEdBQVksY0FBSSxDQUFDLElBQUksQ0FBQyxTQUFTLEVBQUUsSUFBSSxDQUFDLENBQUM7UUFDdEQsT0FBTyxDQUFDLFdBQVcsR0FBUSxjQUFJLENBQUMsSUFBSSxDQUFDLE9BQU8sQ0FBQyxPQUFPLEVBQUUsUUFBUSxFQUFFLFFBQVEsT0FBTyxDQUFDLFVBQVUsRUFBRSxDQUFDLENBQUM7UUFDOUYsT0FBTyxDQUFDLGdCQUFnQixHQUFHLElBQUksd0JBQWEsQ0FBa0I7WUFDMUQsTUFBTSxFQUFnQixlQUFlO1lBQ3JDLG9CQUFvQixFQUFFLElBQUk7WUFDMUIsS0FBSyxFQUFpQixPQUFPLENBQUMsR0FBRyxDQUFDLGdCQUFnQixJQUFJLGNBQUcsQ0FBQyxPQUFPLENBQUMsVUFBVSxDQUFDO1lBQzdFLFVBQVUsRUFBNkI7Z0JBQ25DLGdCQUFnQixFQUFPLEtBQUs7Z0JBQzVCLHFCQUFxQixFQUFFLElBQUk7Z0JBQzNCLHFCQUFxQixFQUFFLEVBQUU7YUFDNUI7U0FDSixDQUFDLENBQUM7UUFDSCxPQUFPLENBQUMsV0FBVyxHQUFRLENBQUMsT0FBTyxDQUFDLE9BQU8sQ0FBQyxJQUFJLENBQUMsUUFBUSxDQUFDLFVBQVUsQ0FBQyxJQUFJLE9BQU8sQ0FBQyxHQUFHLENBQUMsb0JBQW9CLENBQUMsQ0FBQyxDQUFDLENBQUMsNkhBQTZIO0lBQzlPLENBQUM7SUFFTyxNQUFNLENBQUMsT0FBTztRQUVsQixPQUFPLENBQUMsdUJBQXVCLEVBQUUsQ0FBQztRQUNsQyxpQkFBTyxDQUFDLElBQUksRUFBRSxDQUFDO1FBQ2YsdUJBQVUsQ0FBQyxJQUFJLEVBQUUsQ0FBQztRQUNsQixtQkFBUSxDQUFDLElBQUksRUFBRSxDQUFDO1FBQ2hCLE9BQU8sQ0FBQyxpQkFBaUIsRUFBRSxDQUFDO0lBQ2hDLENBQUM7SUFFTSxNQUFNLENBQUMsVUFBVTtRQUVwQixPQUFPLENBQUMsVUFBVSxHQUFHLElBQUksQ0FBQztRQUMxQix1QkFBVSxDQUFDLG9CQUFvQixFQUFFLENBQUM7SUFDdEMsQ0FBQztJQUVNLE1BQU0sQ0FBQyxlQUFlLENBQUMsY0FBdUI7UUFFakQsSUFBSSxPQUFPLENBQUMsVUFBVSxFQUFFO1lBQ3BCLElBQUksT0FBTyxDQUFDLFlBQVksSUFBSSxLQUFLLEVBQUU7Z0JBQy9CLE9BQU8sQ0FBQyxrQkFBa0IsQ0FBQyxjQUFjLENBQUMsQ0FBQzthQUM5QztZQUNELDRKQUE0SjtTQUMvSjtJQUNMLENBQUM7SUFFTyxNQUFNLENBQUMsa0JBQWtCLENBQUMsY0FBdUI7UUFFckQsSUFBSSx3QkFBd0IsR0FBRyxHQUFHLE9BQU8sQ0FBQyxHQUFHLENBQUMsY0FBYyxJQUFJLE9BQU8sQ0FBQyxHQUFHLENBQUMsSUFBSSxzQkFBc0IsT0FBTyxDQUFDLGNBQWMsQ0FBQyxJQUFJLFVBQVUsQ0FBQyxDQUFDLHVHQUF1RztRQUNwUCxJQUFJLHVCQUF1QixHQUFJLEVBQUUsQ0FBQztRQUVsQyxJQUFJLE9BQU8sQ0FBQyxnQkFBZ0IsQ0FBQyxHQUFHLENBQUMsaUJBQWlCLENBQUMsRUFBRTtZQUNqRCx1QkFBdUIsR0FBRyxPQUFPLENBQUMsZ0JBQWdCLENBQUMsR0FBRyxDQUFDLGlCQUFpQixDQUFDLENBQUM7U0FDN0U7YUFBTTtZQUNILElBQUksT0FBTyxDQUFDLEdBQUcsQ0FBQyxzQkFBc0IsRUFBRSxFQUFFLGdGQUFnRjtnQkFDdEgscUJBQXFCO2dCQUNyQix1QkFBdUIsR0FBRyxPQUFPLENBQUMsR0FBRyxDQUFDLHNCQUFzQixDQUFDO2FBQ2hFO2lCQUFNO2dCQUNILG9CQUFvQjtnQkFDcEIsdUJBQXVCLEdBQUcsMkJBQTJCLE9BQU8sQ0FBQyxjQUFjLENBQUMsSUFBSSxVQUFVLENBQUM7YUFDOUY7WUFDRCxPQUFPLENBQUMsZ0JBQWdCLENBQUMsR0FBRyxDQUFDLGlCQUFpQixFQUFFLHVCQUF1QixDQUFDLENBQUM7U0FDNUU7UUFFRCxrQkFBRSxDQUFDLFVBQVUsQ0FBQyx1QkFBdUIsRUFBRSxVQUFVLFFBQWUsRUFBRSwyQkFBb0M7WUFFbEcsSUFBSSxRQUFRLEVBQUU7Z0JBQ1YsT0FBTyxDQUFDLGdCQUFnQixDQUFDLEdBQUcsQ0FBQyxnQkFBZ0IsRUFBRSxLQUFLLENBQUMsQ0FBQztnQkFDdEQsTUFBTSxRQUFRLENBQUM7YUFDbEI7aUJBQU0sSUFBSSwyQkFBMkIsRUFBRTtnQkFDcEMsa0JBQUUsQ0FBQyxVQUFVLENBQUMsd0JBQXdCLEVBQUUsVUFBVSxRQUFlLEVBQUUsNEJBQXFDO29CQUVwRyxJQUFJLFFBQVEsRUFBRTt3QkFDVixPQUFPLENBQUMsZ0JBQWdCLENBQUMsR0FBRyxDQUFDLGdCQUFnQixFQUFFLEtBQUssQ0FBQyxDQUFDO3dCQUN0RCxNQUFNLFFBQVEsQ0FBQztxQkFDbEI7eUJBQU07d0JBQ0gsT0FBTyxDQUFDLDRCQUE0QixHQUFHLElBQUksQ0FBQzt3QkFDNUMsSUFBSSxjQUFjLEVBQUU7NEJBQ2hCLElBQUksQ0FBQyw0QkFBNEIsRUFBRTtnQ0FDL0Isa0JBQUUsQ0FBQyxhQUFhLENBQUMsdUJBQXVCLEVBQUUsd0JBQXdCLEVBQUUsVUFBVSxRQUFlO29DQUV6RixJQUFJLFFBQVEsRUFBRTt3Q0FDVixPQUFPLENBQUMsZ0JBQWdCLENBQUMsR0FBRyxDQUFDLGdCQUFnQixFQUFFLEtBQUssQ0FBQyxDQUFDLENBQUMsMkNBQTJDO3dDQUNsRyxNQUFNLFFBQVEsQ0FBQztxQ0FDbEI7Z0NBQ0wsQ0FBQyxDQUFDLENBQUM7NkJBQ047eUJBQ0o7NkJBQU07NEJBQ0gsSUFBSSw0QkFBNEIsRUFBRTtnQ0FDOUIsa0JBQUUsQ0FBQyxNQUFNLENBQUMsd0JBQXdCLEVBQUUsVUFBVSxRQUFlO29DQUV6RCxJQUFJLFFBQVEsRUFBRTt3Q0FDVixPQUFPLENBQUMsZ0JBQWdCLENBQUMsR0FBRyxDQUFDLGdCQUFnQixFQUFFLElBQUksQ0FBQyxDQUFDLENBQUMsMkNBQTJDO3dDQUNqRyxNQUFNLFFBQVEsQ0FBQztxQ0FDbEI7Z0NBQ0wsQ0FBQyxDQUFDLENBQUM7NkJBQ047eUJBQ0o7cUJBQ0o7Z0JBQ0wsQ0FBQyxDQUFDLENBQUM7YUFDTjtRQUNMLENBQUMsQ0FBQyxDQUFDO0lBQ1AsQ0FBQztJQUVEOztPQUVHO0lBQ0ksTUFBTSxDQUFDLHVCQUF1QjtRQUVqQyxJQUFJLGNBQWMsR0FBRyxjQUFHLENBQUMseUJBQXlCLEVBQUUsQ0FBQztRQUNyRCxJQUFJLGNBQWMsRUFBRTtZQUNoQixjQUFHLENBQUMsRUFBRSxDQUFDLGlCQUFpQixFQUFFO2dCQUV0Qix1QkFBVSxDQUFDLGVBQWUsQ0FBQyxJQUFJLENBQUMsQ0FBQztZQUNyQyxDQUFDLENBQUMsQ0FBQztTQUNOO2FBQU07WUFDSCxjQUFHLENBQUMsSUFBSSxFQUFFLENBQUM7U0FDZDtJQUNMLENBQUM7SUFFTSxNQUFNLENBQUMsaUJBQWlCO1FBRTNCLE9BQU8sQ0FBQyxzQkFBc0IsQ0FBQyxPQUFPLENBQUMsb0JBQW9CLEVBQUU7WUFFekQsdUJBQVUsQ0FBQyxlQUFlLEVBQUUsQ0FBQztRQUNqQyxDQUFDLENBQUMsQ0FBQztRQUNILE9BQU8sQ0FBQyxzQkFBc0IsQ0FBQyxPQUFPLENBQUMsbUJBQW1CLEVBQUU7WUFFeEQsT0FBTyxDQUFDLE9BQU8sRUFBRSxDQUFDO1FBQ3RCLENBQUMsQ0FBQyxDQUFDO0lBQ1AsQ0FBQztJQUVNLE1BQU0sQ0FBQyxPQUFPO1FBRWpCLHVCQUFVLENBQUMsZUFBZSxDQUFDLElBQUksQ0FBQyxDQUFDO1FBQ2pDLHVCQUFVLENBQUMsU0FBUyxDQUFDLFdBQVcsQ0FBQyxJQUFJLENBQUMsU0FBUyxDQUFDLENBQUM7SUFDckQsQ0FBQztJQUVPLE1BQU0sQ0FBQyxzQkFBc0IsQ0FBQyxjQUFzQixFQUFFLFlBQXdCO1FBRWxGLElBQUkseUJBQWMsQ0FBQyxRQUFRLENBQUMsY0FBYyxFQUFFLFlBQVksQ0FBQyxFQUFFO1lBQ3ZELHNDQUFzQztTQUN6QzthQUFNO1lBQ0gsSUFBSSxPQUFPLENBQUMsVUFBVSxFQUFFO2dCQUNwQixPQUFPLENBQUMsVUFBVSxDQUFDLEtBQUssQ0FBQyxnQ0FBZ0MsY0FBYyxFQUFFLENBQUMsQ0FBQzthQUM5RTtZQUNELGlCQUFNLENBQUMsY0FBYyxDQUFDLHVCQUFVLENBQUMsU0FBUyxFQUFFO2dCQUN4QyxJQUFJLEVBQU8sTUFBTTtnQkFDakIsSUFBSSxFQUFPLGNBQUksQ0FBQyxJQUFJLENBQUMsT0FBTyxDQUFDLE9BQU8sRUFBRSxRQUFRLEVBQUUsVUFBVSxDQUFDO2dCQUMzRCxLQUFLLEVBQU0sZ0JBQWdCO2dCQUMzQixPQUFPLEVBQUksK0JBQStCLGNBQWMsRUFBRTtnQkFDMUQsTUFBTSxFQUFLLGtDQUFrQztnQkFDN0MsT0FBTyxFQUFJLENBQUMsV0FBVyxFQUFFLGlCQUFpQixDQUFDO2dCQUMzQyxTQUFTLEVBQUUsQ0FBQzthQUNmLENBQUMsQ0FBQyxJQUFJLENBQUMsVUFBVSxXQUEyQztnQkFFekQsSUFBSSxXQUFXLENBQUMsUUFBUSxLQUFLLENBQUMsRUFBRTtvQkFDNUIsSUFBSSxPQUFPLENBQUMsVUFBVSxFQUFFO3dCQUNwQixPQUFPLENBQUMsVUFBVSxDQUFDLEtBQUssQ0FBQyxvREFBb0QsY0FBYyxFQUFFLENBQUMsQ0FBQztxQkFDbEc7b0JBQ0QsZ0JBQUssQ0FBQyxZQUFZLENBQUMsMkRBQTJELENBQUMsQ0FBQztpQkFDbkY7WUFDTCxDQUFDLENBQUMsQ0FBQztTQUNOO0lBQ0wsQ0FBQzs7QUFyUkwsMEJBdVJDO0FBcFJpQiw0QkFBb0IsR0FBVyxhQUFhLENBQUM7QUFDN0MsMkJBQW1CLEdBQVksYUFBYSxDQUFDO0FBYTdDLG1CQUFXLEdBQW9CLEtBQUssQ0FBQztBQUVyQyxrQkFBVSxHQUFxQixLQUFLLENBQUM7QUFDckMsc0JBQWMsR0FBaUIsMEJBQTBCLENBQUM7QUFDMUQsK0JBQXVCLEdBQVE7SUFDekMsT0FBTyxDQUFDLGNBQWM7SUFDdEIsWUFBWTtJQUNaLDJCQUEyQjtJQUMzQiwwQkFBMEI7SUFDMUIsOEJBQThCO0lBQzlCLDhCQUE4QjtJQUM5Qix5QkFBeUI7SUFDekIscUJBQXFCLEVBQUUsZ0JBQWdCO0NBQzFDLENBQUM7QUFDWSwwQkFBa0IsR0FBYTtJQUN6QyxvQ0FBb0M7SUFDcEMsMkJBQTJCO0lBQzNCLDBCQUEwQjtJQUMxQiw4QkFBOEI7SUFDOUIseUJBQXlCO0lBQ3pCLHFCQUFxQjtDQUN4QixDQUFDO0FBbVBOLE9BQU8sQ0FBQyxJQUFJLEVBQUUsQ0FBQyJ9